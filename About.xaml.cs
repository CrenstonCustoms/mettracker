﻿using System;
using System.Windows;
using System.IO;
using System.Reflection;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            
					 // stores the last modified date as a DateTime type
            DateTime buildDate = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTime;

            string BuildDate = string.Format("Latest build date: {0}", buildDate.ToShortDateString());
            this.BuildString = BuildDate;
            InitializeComponent();
        }

        public static readonly DependencyProperty BuildStringProperty =
        DependencyProperty.Register("BuildString", typeof(string), typeof(About), new UIPropertyMetadata(string.Empty));

        public string BuildString
        {
            get { return (string)GetValue(BuildStringProperty); }
            set { SetValue(BuildStringProperty, value); }
        }

        private void OK_Btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
