﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Data.SQLite;
using CLS = Classes.IO_class;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;
using SQL = SQLiteTrans.SQLiteTransaction;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for AddEquipmentWindow.xaml
    /// </summary>
    public partial class AddEquipmentWindow : Window
    {
        SQLiteConnection m_dbConnection;
        void connectToDatabase()
        {
            string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
        }
        

        String GageSearch;

        void ShowImage()
        {
            try
            {
                ImageSource src = CLS.BitmapFromUri(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + GageTBox.Text + ".jpg", UriKind.Absolute));
                EquipImage.Source = src;
            }
            catch
            {
                EquipImage.Source = new BitmapImage(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\EMPTY.jpg", UriKind.Absolute));
            }
        }

        public AddEquipmentWindow()
        {
            InitializeComponent();
            PasswordEnable();
            ComboBoxFill();

        }

        public List<string> Departments;
        void ComboBoxFill()
        {
            string sql_location = "SELECT DISTINCT Location FROM Equipment";
            Departments = SQL.QueryToListString(sql_location, "Location");
            Departments.Sort();
            Departments.Add("Add New Department");
            LocationTBox.ItemsSource = Departments;
        }

        void PasswordEnable()
        {
            string ReadWriteEnabled = App.Current.Properties["ReadWriteAccess"].ToString();
            if (ReadWriteEnabled == "true")
            {
                AddBtn.IsEnabled = true;
            }
            else
            { }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            GageSearch = GageTBox.Text.ToString();
            connectToDatabase();
            AddToDatabase();
            App.Current.Properties["LoadedImage"] = null;

        }

        void AddToDatabase()
        {
            string sql = "SELECT * FROM Equipment WHERE Gauge = '" + GageSearch + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            string GaugeNumber;
            GaugeNumber = reader["Gauge"].ToString();

            if(GaugeNumber == GageSearch)
            {
                MessageBox.Show("This gauge already exists.");
            }
            else
            {
                MessageBox.Show("This gauge does not exist yet.");
                sql = "insert into Equipment (Gauge, Desc, Owner, Location, Insp, Cert, Due, DevAsRec, Clean, Adj, DevAsRet, Initials, Company, SerialNumber, Comments, Identification, ReferenceDrawing, CalibrationFreq) values ('" + GageTBox.Text + "', '" + DescriptionTBox.Text + "', '" + OwnerTBox.Text + "', '" + LocationTBox.Text + "', '" + InspectedDateTBox.Text + "', '" + 
                    CertTBox.Text + "', '" + DueDateTBox.Text + "', '" + DevAsRecTBox.Text + "', '" + CleanedTBox.Text + "', '" + AdjTBox.Text + "', '" + DevAsRetTBox.Text + "', '" + InitialsTBox.Text + "', '" + CompanyTBox.Text + "', '" + SerialTBox.Text + "', '" + CommentsTBox.Text + "', '" + CodesDetailsTBox.Text + "', '" + RefDwgTBox.Text + "', '" + CalFreqTBox.Text + "')";
                SQLiteCommand commandAdd = new SQLiteCommand(sql, m_dbConnection);
                commandAdd.ExecuteNonQuery();
                MessageBox.Show("Gauge successfully added to the database");

                DescriptionTBox.Text = "";
                OwnerTBox.Text = "";
                LocationTBox.Text = "";
                InspectedDateTBox.Text = "";
                CertTBox.Text = "";
                DueDateTBox.Text = "";
                DevAsRecTBox.Text = "";
                CleanedTBox.Text = "";
                AdjTBox.Text = "";
                DevAsRetTBox.Text = "";
                InitialsTBox.Text = "";
                CompanyTBox.Text = "";
                SerialTBox.Text = "";
                CommentsTBox.Text = "";
                CodesDetailsTBox.Text = "";
                RefDwgTBox.Text = "";
                CalFreqTBox.Text = "";
                GageTBox.Text = "";
            }
        }

        void BadDateMessage(String BadDate)
        {
            MessageBox.Show(string.Format("The date entered in Inspected Date: {0} , is in an unrecognized format." + "\n" + "It should be MM/YYYY.", BadDate));
        }

        private void InspectedDateTBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            InspectedDateTBox.Text = DateTime.Now.ToString("MM/yyyy");
        }
#region radioDateButtons
        private void OneRadio_Click(object sender, RoutedEventArgs e)
        {
            string iString = InspectedDateTBox.Text.ToString();

            if (iString.Length == 6)
            {
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(1);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 7)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/yyyy", null);
                oDate = oDate.AddMonths(1);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 10)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/dd/yyyy", null);
                oDate = oDate.AddMonths(1);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            else { BadDateMessage(iString); }

        End:;
        }

        private void TwelveRadio_Click(object sender, RoutedEventArgs e)
        {
            string iString = InspectedDateTBox.Text.ToString();

            if (iString.Length == 6)
            {
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(12);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 7)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/yyyy", null);
                oDate = oDate.AddMonths(12);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 10)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/dd/yyyy", null);
                oDate = oDate.AddMonths(12);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            else { BadDateMessage(iString); }

        End:;
        }

        private void SixRadio_Click(object sender, RoutedEventArgs e)
        {
            string iString = InspectedDateTBox.Text.ToString(); ;

            if (iString.Length == 6)
            {
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(6);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 7)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/yyyy", null);
                oDate = oDate.AddMonths(6);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 10)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/dd/yyyy", null);
                oDate = oDate.AddMonths(6);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            else { BadDateMessage(iString); }

        End:;
        }
#endregion

        // needs work	11/29/17
        private void ImageRightClick(object sender, RoutedEventArgs e)
        {
            if(GageTBox.Text == "") { MessageBox.Show("You must enter a Gauge ID first.");return; }
            CLS.ImportImage(GageTBox.Text);
            ShowImage();
        }

        private void LocationTBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.IsMouseCaptured)
            { return; }

            if (LocationTBox.SelectedItem.ToString() == "Add New Department")
            {
                App.Current.Properties["NewLocation"] = "";
                TextMessageBox textMessageBox = new TextMessageBox();
                textMessageBox.Owner = App.Current.MainWindow;
                textMessageBox.Title = "Add Location";
                textMessageBox.ShowDialog();
                string tempString = App.Current.Properties["NewLocation"].ToString();
                Departments.Add(tempString);
                LocationTBox.Items.Refresh();
                if (tempString != "") { LocationTBox.SelectedItem = tempString; }
            }
        }
    }
}
