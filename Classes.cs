﻿using System;
using System.Windows;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.Windows.Controls;

namespace Classes
{
    public class IO_class
    {
        public static bool DirectoryAccessCheck(string Directory)
        {
            FileInfo fi = new FileInfo(Directory);
            bool IsReadOnly = fi.IsReadOnly;
            return IsReadOnly;
        }

        public static int GBC_MaxStepSetting = 0;

        public static List<string> readIt(string fileName)
        {
            string line;
            List<string> data = new List<string>();
            StreamReader file = new StreamReader(fileName);
            while ((line = file.ReadLine()) != null)
            { if (line != "") data.Add(line); }
            file.Close();
            return data;
        }

        public static string GetSettingsStrings(string SettingID)
        {
            string SettingsString = "";
            switch(SettingID)
            {
                case "DBDirectory":
                    SettingsString = MetTracker.Properties.Settings.Default.DBDirectory.ToString();
                    break;
                case "DWGDirectory":
                    SettingsString = MetTracker.Properties.Settings.Default.DWGDirectory.ToString();
                    break;
                case "DWGTable":
                    SettingsString = MetTracker.Properties.Settings.Default.DWGTable.ToString();
                    break;
                case "SOPCalDirectory":
                    SettingsString = MetTracker.Properties.Settings.Default.SOPCalDirectory.ToString();
                    break;
                case "SOPDirectory":
                    SettingsString = MetTracker.Properties.Settings.Default.SOPDirectory.ToString();
                    break;
                case "WIDirectory":
                    SettingsString = MetTracker.Properties.Settings.Default.WIDirectory.ToString();
                    break;
                case "GBCMaxStep":
                    GBC_MaxStepSetting = MetTracker.Properties.Settings.Default.GBC_MaxStep;
                    break;
                case "ExternalCalInterval":
                    SettingsString = MetTracker.Properties.Settings.Default.ExternalCalInterval.ToString();
                    break;
                case "Cal_Cert_Directory":
                    SettingsString = MetTracker.Properties.Settings.Default.Cal_Cert_Directory.ToString();
                    break;

            }

            


            return SettingsString;
        }

        public static class Settings
        {
            public static string CalCertDirectory
            {
                get{ return MetTracker.Properties.Settings.Default.Cal_Cert_Directory.ToString(); }
                set{ MetTracker.Properties.Settings.Default.Cal_Cert_Directory = value; }
            }
            public static double ExternalCalInterval
            {
                get { return double.Parse(MetTracker.Properties.Settings.Default.ExternalCalInterval.ToString()); }
                set { MetTracker.Properties.Settings.Default.ExternalCalInterval = value; }
            }
        }


#region Gauge Block functions
        public static void CountSum(decimal[] DNumbers, decimal Dsum)
        {

            foreach (Window window in Application.Current.Windows)
            {
                if (window.GetType() == typeof(MetTracker.GaugeCalc))
                {
                    (window as MetTracker.GaugeCalc).CalculateBtn.Content = "working...";
                }
            }

            DNumbers = Array.ConvertAll(DNumbers, element => 10000m * element);
            string TempString = GetSettingsStrings("GBCMaxStep"); // only used to initialize max step value
            Dsum = Dsum * 10000m;
            Int32 Isum = Convert.ToInt32(Dsum);
            Int32[] INumbers = Array.ConvertAll(DNumbers, element => (Int32)element);
            // int result = 0;
                GetmNumberOfSubsets(INumbers, Isum);
            success = false;
            return;
        }

        private static void GetmNumberOfSubsets(Int32[] numbers, Int32 Isum)
        {
            set = numbers;
            sum = Isum;
            FindSubsetSum();
        }
        //-------------------------------------------------------------

        static Int32[] set;
        static Int32[] subSetIndexes;
        static Int32 sum;
        static Int32 numberOfSubsetSums;
        static bool success = false;
        static List<Int32> ResultSet = new List<Int32>();
        
        static List<string> results = new List<string>();//------------------------------------------------------------

        /*
            Method: FindSubsetSum()
        */
        private static void FindSubsetSum()
        {
            numberOfSubsetSums = 0;
            Int32 numberOfElements = set.Length;
            FindPowerSet(numberOfElements);
        }
        //-------------------------------------------------------------

        /*
            Method: FindPowerSet(int n, int k)
        */
        private static void FindPowerSet(Int32 n)
        {
            // Super set - all sets with size: 0, 1, ..., n - 1
            for (Int32 k = 0; k <= n - 1; k++)
            {
                subSetIndexes = new Int32[k];
                CombinationsNoRepetition(k, 0, n - 1);
                if(subSetIndexes.Length >= GBC_MaxStepSetting) {
                    break; }
            }

            if (numberOfSubsetSums == 0)
            {
                MessageBox.Show("No subsets with wanted sum exist.");
            }
        }
        //-------------------------------------------------------------

        /*
            Method: CombinationsNoRepetition(int k, int iBegin, int iEnd);
        */
        private static void CombinationsNoRepetition(Int32 k, Int32 iBegin, Int32 iEnd)
        {
            if (k == 0)
            {
                PrintSubSet();
                return;
            }

            if (success == false)
            {
                for (Int32 i = iBegin; i <= iEnd; i++)
                {
                    subSetIndexes[k - 1] = i;
                    ++iBegin;
                    CombinationsNoRepetition(k - 1, iBegin, iEnd);
                    if (success == true)
                        break;
                }
                
            }
                return;
        }

        private static void PrintSubSet()
        {
            Int32 currentSubsetSum = 0;

            // accumulate sum of current subset
            for (Int32 i = 0; i < subSetIndexes.Length; i++)
            {
                currentSubsetSum += set[subSetIndexes[i]];
                if(currentSubsetSum > sum) { break; }
            }

            if(currentSubsetSum > sum) { return; }
            
            // if wanted sum: print current subset elements
            if (currentSubsetSum == sum)
            {
                ++numberOfSubsetSums;

                // results.Add("(");
                for (Int32 i = 0; i < subSetIndexes.Length; i++)
                {
                   results.Add((set[subSetIndexes[i]]).ToString());
                    ResultSet.Add(set[subSetIndexes[i]]);
                    if (i < subSetIndexes.Length - 1)
                    {
                        // results.Add(" ,");
                    }
                }
                // results.Add(")");
                Int32[] ResultSetArr = ResultSet.ToArray();
                decimal[] ResultSetArrD = Array.ConvertAll(ResultSetArr, element => (decimal)element);
                ResultSetArrD = Array.ConvertAll(ResultSetArrD, element => element / 10000m);
                // var message = string.Join(Environment.NewLine, ResultSetArrD);
                // message = string.Format("{0:0.0000}", message);
                int l = ResultSetArrD.Length;
                string[] ResultString = new string[l];
                foreach(int i in ResultSetArrD)
                {ResultString = Array.ConvertAll(ResultSetArrD, element => element.ToString("0.0000"));}
                var message = string.Join(Environment.NewLine, ResultString);
                decimal ResultSum = ResultSetArrD.Sum();
                MessageBox.Show(message + "\n= " + ResultSum.ToString("0.0000"), "Result");
                Array.Clear(ResultSetArrD, 0, ResultSetArrD.Length);
                Array.Clear(ResultSetArr, 0, ResultSetArr.Length);
                ResultSet.Clear();
                message = null;
                success = true;
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.GetType() == typeof(MetTracker.GaugeCalc))
                    {
                        (window as MetTracker.GaugeCalc).CalculateBtn.Content = "Calculate";
                    }
                }
                return;
            }

            if (success == true)
                return;
        }
        #endregion

#region Image import functions
        static public void ImportImage(string GaugeNumber)
        {
            OpenFileDialog op = new OpenFileDialog
            {
                Title = "Select an image",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg"
            };
            if (op.ShowDialog() == true)
            {
                byte[] imageBytes = LoadImageData(op.FileName);
                ImageSource imageSource = CreateImage(imageBytes, 360, 270);
                imageBytes = GetEncodedImageData(imageSource, ".jpg");

                string CTIsopDirectory;
                string ImportNumberParsed = String.Empty;
                ImportNumberParsed = GaugeNumber;
                if (ImportNumberParsed == "System.Windows.Controls.TextBox")
                {
                    MessageBox.Show("You must enter an ID for the equipment image to import.\nThis ID is case sensitve.");
                }
                else
                {
                    CTIsopDirectory = @"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + ImportNumberParsed + ".jpg";
                    File.Delete(op.FileName);
                    CTIsopDirectory = @"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + ImportNumberParsed + ".jpg";
                    SaveImageData(imageBytes, CTIsopDirectory);
                    
                }
            }


        }

        private static byte[] LoadImageData(string filePath)

        {
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] imageBytes = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();
            return imageBytes;
        }

        private static void SaveImageData(byte[] imageData, string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(imageData);
            bw.Close();
            fs.Close();
        }

        private static ImageSource CreateImage(byte[] imageData, int decodePixelWidth, int decodePixelHeight)
        {
            if (imageData == null) return null;

            BitmapImage result = new BitmapImage();
            result.BeginInit();
            if (decodePixelWidth > 0)
            {
                result.DecodePixelWidth = decodePixelWidth;
            }

            if (decodePixelHeight > 0)
            {
                result.DecodePixelHeight = decodePixelHeight;
            }

            result.StreamSource = new MemoryStream(imageData);
            result.CreateOptions = BitmapCreateOptions.None;
            result.CacheOption = BitmapCacheOption.Default;
            result.EndInit();
            return result;
        }

        static internal byte[] GetEncodedImageData(ImageSource image, string preferredFormat)
        {
            byte[] result = null;
            BitmapEncoder encoder = null;

            switch (preferredFormat.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    encoder = new JpegBitmapEncoder();
                    break;

                case ".bmp":
                    encoder = new BmpBitmapEncoder();
                    break;

                case ".png":
                    encoder = new PngBitmapEncoder();
                    break;

                case ".tif":
                case ".tiff":
                    encoder = new TiffBitmapEncoder();
                    break;

                case ".gif":
                    encoder = new GifBitmapEncoder();
                    break;

                case ".wmp":
                    encoder = new WmpBitmapEncoder();
                    break;
            }

            if (image is BitmapSource)
            {
                MemoryStream stream = new MemoryStream();
                encoder.Frames.Add(BitmapFrame.Create(image as BitmapSource));
                encoder.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                result = new byte[stream.Length];
                BinaryReader br = new BinaryReader(stream);
                br.Read(result, 0, (int)stream.Length);
                br.Close();
                stream.Close();
            }

            return result;
        }
        #endregion

#region parse text from control string result
        public static string cboParser(string controlString)
        {
            if (controlString.Contains(':'))
            {
                controlString = controlString.Split(':')[1].TrimStart(' ');
            }
            return controlString;
        }
        #endregion

        public static ImageSource BitmapFromUri(Uri source)
        {
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = source;
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
        }
    }

    public class Control_Class
    {
    }
        
    
}