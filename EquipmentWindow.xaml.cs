﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Data.SQLite;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Controls;

using SQL = SQLiteTrans.SQLiteTransaction;
using CLS = Classes.IO_class;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for EquipmentWindow.xaml
    /// </summary>
    public partial class EquipmentWindow : Window
    {
        SQLiteConnection m_dbConnection;
        void connectToDatabase()
        {
            // m_dbConnection = new SQLiteConnection("Data Source=SOP7_6.db;Version=3;");
            // m_dbConnection.Open();
            string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
        }
        // Writes the Gauge number, Type, and Date in a MessageBox depending on the Gauge number in the QueryTxt TextBox.

        string OrigDueDate;
        List<string> Departments;
        bool UpdateEnabled = false;

        void PrintGauges()  // works
        {

            string sql_location = "SELECT DISTINCT Location FROM Equipment";
            Departments = SQL.QueryToListString(sql_location, "Location");
            Departments.Sort();
            Departments.Add("Add New Department");
            LocationTBox.ItemsSource = Departments;

            try { 
            string sql = "SELECT * FROM Equipment WHERE Gauge = '" + GageSearch + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            string GaugeNumber;
            GaugeNumber = reader["Gauge"].ToString();
                

                GageTBox.Text = GaugeNumber;
                DescriptionTBox.Text = reader["Desc"].ToString();
                OwnerTBox.Text = reader["Owner"].ToString();
                LocationTBox.Text = reader["Location"].ToString();
                InspectedDateTBox.Text = reader["Insp"].ToString();
                CertTBox.Text = reader["Cert"].ToString();
                DueDateTBox.Text = reader["Due"].ToString();
                OrigDueDate = reader["Due"].ToString();
                DevAsRecTBox.Text = reader["DevAsRec"].ToString();
                CleanedTBox.Text = reader["Clean"].ToString();
                AdjTBox.Text = reader["Adj"].ToString();
                DevAsRetTBox.Text = reader["DevAsRet"].ToString();
                FitForUseCB.Text = reader["FitForUse"].ToString();
                InitialsTBox.Text = reader["Initials"].ToString();
                CompanyTBox.Text = reader["Company"].ToString();
                SerialTBox.Text = reader["SerialNumber"].ToString();
                CommentsTBox.Text = reader["Comments"].ToString();
                CodesDetailsTBox.Text = reader["Identification"].ToString();
                RefDwgTBox.Text = reader["ReferenceDrawing"].ToString();
                CalFreqTBox.Text = reader["CalibrationFreq"].ToString();
                ShowImage();
                UpdateEnabled = true;
            }
            catch
            {
                
                    MessageBox.Show("The Gauge " + GageSearch + " does not exist.");
                    this.Close();
                
            }

            m_dbConnection.Close();

        }

        String GageSearch;

        void ShowImage()
        {
            try
            {
                // EquipImage.Source = new BitmapImage(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + GageTBox.Text + ".jpg", UriKind.Absolute));
                ImageSource src = CLS.BitmapFromUri(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + GageTBox.Text + ".jpg", UriKind.Absolute));
                EquipImage.Source = src;
            }
            catch
            {
                // EquipImage.Source = new BitmapImage(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\EMPTY.jpg", UriKind.Absolute));
                EquipImage.Source = new BitmapImage(new Uri("EMPTY.jpg", UriKind.Relative));
            }
        }

        public EquipmentWindow(String _GageSearch)
        {
            connectToDatabase();
            GageSearch = _GageSearch;
            InitializeComponent();
            PasswordEnable();
            PrintGauges();

            
            
        }

        void PasswordEnable()
        {
            string ReadWriteEnabled = App.Current.Properties["ReadWriteAccess"].ToString();
            if (ReadWriteEnabled == "true")
            {
                // AddBtn.IsEnabled = true;
                UpdateBtn.IsEnabled = true;
            }
            else
            { }
        }

        void BadDateMessage(String BadDate)
        {
            MessageBox.Show(string.Format("The date entered in Inspected Date: {0} , is in an unrecognized format." + "\n" + "It should be MM/YYYY.", BadDate));
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        
        // Enters todays date as MM/YYYY. If Calibration frequency is entered, used to also set due date.
        private void InspectedDateTBox_MouseDoubleClick(object sender, MouseButtonEventArgs e) 
        {
            InspectedDateTBox.Text = DateTime.Now.ToString("MM/yyyy");
            int CalFreq = 0;
            try { CalFreq = int.Parse(CalFreqTBox.Text); }
            catch { }

            if (CalFreq != 0)
            {
                CustomRadio.IsChecked = true;
                string iString = InspectedDateTBox.Text.ToString();
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(CalFreq);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
            }
        }

        private void OneRadio_Click(object sender, RoutedEventArgs e)
        {
            string iString = InspectedDateTBox.Text.ToString();

            if (iString.Length == 6)
            {
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(1);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 7)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/yyyy", null);
                oDate = oDate.AddMonths(1);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 10)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/dd/yyyy", null);
                oDate = oDate.AddMonths(1);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            else { BadDateMessage(iString); }

            End:;
        }

        private void TwelveRadio_Click(object sender, RoutedEventArgs e)
        {
            string iString = InspectedDateTBox.Text.ToString();

            if (iString.Length == 6)
            {
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(12);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 7)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/yyyy", null);
                oDate = oDate.AddMonths(12);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 10)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/dd/yyyy", null);
                oDate = oDate.AddMonths(12);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            else { BadDateMessage(iString); }

        End:;
        }

        private void SixRadio_Click(object sender, RoutedEventArgs e)
        {
            string iString = InspectedDateTBox.Text.ToString(); ;

            if (iString.Length == 6)
            {
                DateTime oDate = DateTime.ParseExact(iString, "M/yyyy", null);
                oDate = oDate.AddMonths(6);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 7)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/yyyy", null);
                oDate = oDate.AddMonths(6);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            if (iString.Length == 10)
            {
                DateTime oDate = DateTime.ParseExact(iString, "MM/dd/yyyy", null);
                oDate = oDate.AddMonths(6);
                DueDateTBox.Text = oDate.ToString("MM/yyyy");
                goto End;
            }
            else { BadDateMessage(iString); }

        End:;
        }

        public void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {

            if (UpdateEnabled)
            {
                if ((FitForUseCB.Text != "Yes") && (FitForUseCB.Text != "No"))
                {
                    MessageBox.Show("\"Fit for use\" has not been updated!");
                    return;
                }

                m_dbConnection.Close();
                connectToDatabase();
                string sql = "UPDATE Equipment SET " +
                    "Desc='" + DescriptionTBox.Text + "', " +
                    "Owner='" + OwnerTBox.Text + "', " +
                    "Location='" + LocationTBox.Text + "', " +
                    "Insp='" + InspectedDateTBox.Text + "', " +
                    "Cert='" + CertTBox.Text + "', " +
                    "Due='" + DueDateTBox.Text + "', " +
                    "DevAsRec='" + DevAsRecTBox.Text + "', " +
                    "Clean='" + CleanedTBox.Text + "', " +
                    "Adj='" + AdjTBox.Text + "', " +
                    "DevAsRet='" + DevAsRetTBox.Text + "', " +
                    "Initials='" + InitialsTBox.Text + "', " +
                    "Company='" + CompanyTBox.Text + "', " +
                    "SerialNumber='" + SerialTBox.Text + "', " +
                    "Comments='" + CommentsTBox.Text + "', " +
                    "Identification='" + CodesDetailsTBox.Text + "', " +
                    "ReferenceDrawing='" + RefDwgTBox.Text + "', " +
                    "FitForUse='" + FitForUseCB.Text + "', " +
                    "CalibrationFreq='" + CalFreqTBox.Text + "' " +
                    "where Gauge='" + GageTBox.Text + "'";

                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                m_dbConnection.Close();

                SQL.AddHistory(GageTBox.Text, OwnerTBox.Text, LocationTBox.Text, DueDateTBox.Text, InspectedDateTBox.Text, DevAsRecTBox.Text, DevAsRetTBox.Text, CommentsTBox.Text, CalFreqTBox.Text, InitialsTBox.Text, OrigDueDate, FitForUseCB.Text, CertTBox.Text);

                DescriptionTBox.Text = "";
                OwnerTBox.Text = "";
                LocationTBox.Text = "";
                InspectedDateTBox.Text = "";
                CertTBox.Text = "";
                DueDateTBox.Text = "";
                DevAsRecTBox.Text = "";
                CleanedTBox.Text = "";
                AdjTBox.Text = "";
                DevAsRetTBox.Text = "";
                InitialsTBox.Text = "";
                CompanyTBox.Text = "";
                SerialTBox.Text = "";
                CommentsTBox.Text = "";
                CodesDetailsTBox.Text = "";
                RefDwgTBox.Text = "";
                CalFreqTBox.Text = "";
                GageTBox.Text = "";
                FitForUseCB.SelectedItem = "";
                EquipImage.Source = new BitmapImage(new Uri("EMPTY.jpg", UriKind.Relative));
                UpdateEnabled = false;
            }
            else
            { MessageBox.Show("You must search for a Gauge first!"); }
        }

        private void GageTBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            connectToDatabase();
            GageSearch = GageTBox.Text;
            PrintGauges();
            m_dbConnection.Close();
        }

        private void LocationTBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.IsMouseCaptured)
            { return; }

            if (LocationTBox.SelectedItem.ToString() == "Add New Department")
            {
                App.Current.Properties["NewLocation"] = "";
                TextMessageBox textMessageBox = new TextMessageBox();
                textMessageBox.Owner = App.Current.MainWindow;
                textMessageBox.Title = "Add Location";
                textMessageBox.ShowDialog();
                string tempString = App.Current.Properties["NewLocation"].ToString();
                Departments.Add(tempString);
                LocationTBox.Items.Refresh();
                if (tempString != "") { LocationTBox.SelectedItem = tempString; }
            }
        }

        private void CM_LocationTBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteTrans.SQLiteTransaction.ConnectToDatabase();
                string sql = "SELECT Gauge FROM Vendors WHERE Gauge = '" + GageTBox.Text + "'";
                string GaugeNumber;
                GaugeNumber = SQL.QueryDB(sql, "Gauge");
                SQLiteTrans.SQLiteTransaction.disconnectFromDB();

                if (GaugeNumber == "") { Exception exception = new Exception(); throw exception; } // Check if Gauge exists in Vendors table. If not, goto catch.

                sql = "UPDATE Vendors SET " +
                      "Type='" + DescriptionTBox.Text + "', " +
                      "VendorID='" + LocationTBox.Text + "' " +
                      "where Gauge='" + GageTBox.Text + "'";
                connectToDatabase();
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                m_dbConnection.Close();

                MessageBox.Show(string.Format("Gauge {0} was updated in the vendors equipment list, \nunder {1}.", GageTBox.Text, LocationTBox.Text));
            }
            catch
            {
                string sql = "INSERT INTO Vendors (Gauge, VendorID, Type) values ('" + GageTBox.Text + "', '" + LocationTBox.Text + "', '" + DescriptionTBox.Text + "')";
                connectToDatabase();
                SQLiteCommand commandAdd = new SQLiteCommand(sql, m_dbConnection);
                commandAdd.ExecuteNonQuery();
                m_dbConnection.Close();

                MessageBox.Show(string.Format("Gauge {0} was added to the Vendors equipment tracking, \nunder {1}.", GageTBox.Text, LocationTBox.Text));
            }
        }

        private void CertTBox_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            EquipmentWindow equipmentWindow = new EquipmentWindow(CertTBox.Text);
            equipmentWindow.Owner = App.Current.MainWindow;
            equipmentWindow.ShowDialog();
        }

        private void EquipImage_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            CLS.ImportImage(GageTBox.Text);
            ShowImage();
        }

        private void FitForUseCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.IsMouseCaptured)
            { return; }
        }
    }
}
