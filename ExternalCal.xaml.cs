﻿using System.Windows;
using SQL = SQLiteTrans.SQLiteTransaction;
using System.Data;
using CLS = Classes.IO_class;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls.Primitives;
using System.IO;
using Microsoft.Win32;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for ExternalCal.xaml
    /// </summary>
    public partial class ExternalCal : Window
    {
        public ExternalCal(DataTable ExternalDue)
        {
            InitializeComponent();
            if (ExternalDue != null) { FutureDue = ExternalDue.Copy(); }
            FillListing();
            CalHouse_ComboBox_Fill();
            if(ReadWriteEnabled == "false")
            {
                Add_Equip_Btn.IsEnabled = false;
                Add_CalHouse_Btn.IsEnabled = false;
            }
            
        }

        DataTable FutureDue = new DataTable();

        #region // Public variables for interaction between methods
        DataTable TempDT = new DataTable();
        int SelectedIDIndex;
        string[] IndData;
        string SelectedEquipID = "";
        string ReadWriteEnabled = App.Current.Properties["ReadWriteAccess"].ToString();
        #endregion

        #region // Equipment Tab        
        private void FillListing() // Populates the datagrid on the "Equipment" tab from the database
        {
            if(FutureDue.Rows.Count != 0)
            {
                
                TempDT = FutureDue.Copy();
                DataColumnCollection collection = FutureDue.Columns;
                if (collection.Contains("SPECS")) { FutureDue.Columns.Remove("SPECS"); }
                EquipListDG.ItemsSource = FutureDue.DefaultView;
            }

            if (FutureDue.Rows.Count == 0)
            { 
            string sql = "SELECT * FROM External";
            DataTable dataTable = new DataTable();
            dataTable = SQL.QueryToDataTable(sql);
            TempDT = dataTable.Copy();
            dataTable.Columns.Remove("SPECS");
            EquipListDG.ItemsSource = dataTable.DefaultView;
            }
        }
        
				// prevents user from editing datagrid cell contents. a form of readonly protection
        private void resultGrid_CellEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (this.EquipListDG.SelectedItem != null)
            {
                (sender as DataGrid).RowEditEnding -= resultGrid_CellEditEnding;
                (sender as DataGrid).CancelEdit();
                (sender as DataGrid).Items.Refresh();
                (sender as DataGrid).RowEditEnding += resultGrid_CellEditEnding;

            }
        }

        private void EquipListDG_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (EquipListDG.CurrentColumn.Header.ToString() == "ID")
            {
                SelectedIDIndex = 0;

                object item = EquipListDG.SelectedItem;
                string ID = EquipListDG.CurrentColumn.GetCellContent(item).ToString();

                ID = CLS.cboParser(ID);
                if ((ID == "System.Windows.Controls.TextBox") || (ID == "System.Windows.Controls.TextBlock")) { return; }
                SelectedEquipID = ID;
                DataRow row = TempDT.Select(string.Format("ID = '" + ID + "'"))[0];
                int SelectedRow = TempDT.Rows.IndexOf(row);
                SelectedIDIndex = SelectedRow;
                StripSpecs();
                Details.Focus();
            }
            if (EquipListDG.CurrentColumn == null) { return; }
            if (EquipListDG.CurrentColumn.Header.ToString() == "CAL_HOUSE")
            {
                SelectedIDIndex = 0;

                object item = EquipListDG.SelectedItem;
                string CalHouse = EquipListDG.CurrentColumn.GetCellContent(item).ToString();

                CalHouse = CLS.cboParser(CalHouse);
                if ((CalHouse == "System.Windows.Controls.TextBox") || (CalHouse == "System.Windows.Controls.TextBlock")) { return; }
                CalHouseTab.Focus();
                try
                {
                    int index = CalHouse_CB.Items.IndexOf(CalHouse);
                    if (index == -1)
                    {
                        throw new Exception();
                    }
                    CalHouse_CB.SelectedIndex = index;
                }
                catch
                {
                    MessageBox.Show(string.Format("The Calibration House {0} is entered incorrectly\nor was not found.", CalHouse));
                }
            }
        }

        private void Equipment_GotFocus(object sender, RoutedEventArgs e)
        {

            if (sender as DataGrid != null)
            {
                if (this.EquipListDG.SelectedItem != null)
                {
                    (sender as DataGrid).Items.Refresh();
                }
            }
        }
        #endregion

        #region // Details Tab

        private void Details_GotFocus(object sender, RoutedEventArgs e)
        {
            // StripSpecs();
        }
        private void StripSpecs() // Separates the individual specs from the concatenated string
        {

            if (IndData != null) { Array.Clear(IndData, 0, IndData.Length); }
            string SpecsData = "";
            SpecsData = TempDT.Rows[SelectedIDIndex].Field<string>("SPECS");
            DetailsGrid.Children.Clear();
            DetailsGrid.RowDefinitions.Clear();
            if ((SpecsData == null)|(SpecsData == " ")) // checks for empty string
            {
                object item = EquipListDG.SelectedItem;
                string ID = EquipListDG.CurrentColumn.GetCellContent(item).ToString();
                ID = CLS.cboParser(ID);
                Label label = new Label();
                label.Content = string.Format("No specs entered for {0}", ID);
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(25);
                DetailsGrid.RowDefinitions.Add(row);
                Grid.SetRow(label, 0);
                Grid.SetColumnSpan(label, 2);
                DetailsGrid.Children.Add(label);
                RowDefinition row1 = new RowDefinition();
                row1.Height = new GridLength(25);
                DetailsGrid.RowDefinitions.Add(row1);
                Button button_Edit = new Button();
                if(App.Current.Properties["ReadWriteAccess"].ToString() == "false")
                {
                    button_Edit.IsEnabled = false;
                }
                button_Edit.Content = "Edit";
                button_Edit.Click += delegate
                {
                    New_External new_External = new New_External(true, SelectedEquipID);
                    new_External.Owner = App.Current.MainWindow;
                    new_External.ShowDialog();
                    MessageBox.Show("To show any changes made, this window must be closed, and reopened.");
                    Equipment.Focus();
                };
                Grid.SetColumn(button_Edit, 0);
                Grid.SetRow(button_Edit, DetailsGrid.RowDefinitions.Count - 1);
                DetailsGrid.Children.Add(button_Edit);
                RowDefinition row2 = new RowDefinition();
                row2.Height = new GridLength(25);
                DetailsGrid.RowDefinitions.Add(row2);
                Button Certs = new Button();
                Certs.Content = "View Cal. Certs.";
                Certs.Click += delegate
                {
                    OpenFileDialog op = new OpenFileDialog
                    {
                        InitialDirectory = "",
                        Title = "Select a Cert",
                        Filter = "Cert Documents|"+ SelectedEquipID + "*.jpg;" + SelectedEquipID + "*.jpeg;" + SelectedEquipID + "*.pdf"
                        //"JPEG (" + SelectedEquipID + "*.jpg;" + SelectedEquipID + "*.jpeg)|" + SelectedEquipID + "*.jpg;" + SelectedEquipID + "*.jpeg" 
                        //+ SelectedEquipID + "*.pdf|" + "PDF (" + SelectedEquipID + "*.pdf)|" + SelectedEquipID + "*.pdf" 
                    };
                    if (op.ShowDialog() == true)
                    {
                        try
                        {
                            System.Diagnostics.Process process = new System.Diagnostics.Process();
                            process.StartInfo.FileName = op.FileName;
                            process.Start();
                            process.WaitForExit();
                        }
                        catch
                        {

                        }
                    }
                    };
                Grid.SetColumn(Certs, 0);
                Grid.SetRow(Certs, DetailsGrid.RowDefinitions.Count - 1);
                DetailsGrid.Children.Add(Certs);

                Image image = new Image();
                image.Width = 360;
                image.Height = 270;
                image.ClipToBounds = false;
                image.HorizontalAlignment = HorizontalAlignment.Stretch;
                image.VerticalAlignment = VerticalAlignment.Stretch;
                Canvas canvas = new Canvas();
                canvas.Width = 360;
                canvas.Height = 270;
                canvas.Children.Add(image);
                try
                {
                    ImageSource src = CLS.BitmapFromUri(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + SelectedEquipID + ".jpg", UriKind.Absolute));
                    image.Source = src;
                }
                catch
                {
                    ImageSource src = CLS.BitmapFromUri(new Uri("EMPTY.jpg", UriKind.Relative));
                    image.Source = src;
                }
                Grid.SetColumn(canvas, 2);
                Grid.SetRow(canvas, 0);
                DetailsGrid.Children.Add(canvas);

                return;
            }
            IndData = Regex.Split(SpecsData, ";");
            FillDetailsGrid();
        }

        private void FillDetailsGrid()
        {
            DetailsGrid.RowDefinitions.Clear();
            Image image = new Image();
            image.Width = 360;
            image.Height = 270;
            image.ClipToBounds = false;
            image.HorizontalAlignment = HorizontalAlignment.Stretch;
            image.VerticalAlignment = VerticalAlignment.Stretch;
            Canvas canvas = new Canvas();
            canvas.Width = 360;
            canvas.Height = 270;
            canvas.Children.Add(image);
            try
            {
                ImageSource src = CLS.BitmapFromUri(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + SelectedEquipID + ".jpg", UriKind.Absolute));
                image.Source = src;
            }
            catch
            {
                ImageSource src = CLS.BitmapFromUri(new Uri("EMPTY.jpg", UriKind.Relative));
                image.Source = src;
            }
            Grid.SetColumn(canvas, 2);
            Grid.SetRow(canvas, 0);
            DetailsGrid.Children.Add(canvas);

            RowDefinition IDrow = new RowDefinition();
            IDrow.Name = "IDLabel";
            IDrow.Height = new GridLength(25);
            DetailsGrid.RowDefinitions.Add(IDrow);
            Label IDLabel = new Label();
            IDLabel.Content = string.Format("Equipment ID: {0}", SelectedEquipID);
            Grid.SetColumnSpan(IDLabel, 2);
            IDLabel.HorizontalAlignment = HorizontalAlignment.Left;
            DetailsGrid.Children.Add(IDLabel);

            int IndDataLength = IndData.Length;
            for(int i = 0; i < IndDataLength; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Name = string.Format("row{0}", i);
                row.Height = new GridLength(25);
                DetailsGrid.RowDefinitions.Add(row);
            }

            int j = 1;
            foreach (string value in IndData)
            {
                if (value == "") { continue; }
                string[] SplitData = Regex.Split(value, ":");
                if (SplitData.Length < 2) { image.Source = null; return; }
                Label label = new Label();
                label.Content = SplitData[0];
                Grid.SetColumn(label, 0);
                Grid.SetRow(label, j);
                TextBox textBox = new TextBox();
                textBox.Text = SplitData[1];
                Grid.SetColumn(textBox, 1);
                Grid.SetRow(textBox, j);
                DetailsGrid.Children.Add(label);
                DetailsGrid.Children.Add(textBox);
                j++;  
            }
            RowDefinition row1 = new RowDefinition();
            row1.Height = new GridLength(25);
            DetailsGrid.RowDefinitions.Add(row1);
            Button button_Edit = new Button();
            button_Edit.Content = "Edit";
            if(App.Current.Properties["ReadWriteAccess"].ToString() == "false")
            {
                button_Edit.IsEnabled = false;
            }
            button_Edit.Click += delegate
            {
                New_External new_External = new New_External(true, SelectedEquipID);
                new_External.Owner = App.Current.MainWindow;
                new_External.ShowDialog();
                FillListing();
                Equipment.Focus();
            };
            Grid.SetColumn(button_Edit, 0);
            Grid.SetRow(button_Edit, DetailsGrid.RowDefinitions.Count - 1);
            DetailsGrid.Children.Add(button_Edit);
            RowDefinition row2 = new RowDefinition();
            row2.Height = new GridLength(25);
            DetailsGrid.RowDefinitions.Add(row2);
            Button Certs = new Button();
            Certs.Content = "View Cal. Certs.";
            Certs.Click += delegate
            {
                OpenFileDialog op = new OpenFileDialog
                {
                    InitialDirectory = @"V:\Users\GAGE CALIBRATION RECORDS\Cal_Certs",
                Title = "Select a Cert",
                    Filter = "Cert Documents|" + SelectedEquipID + "*.jpg;" + SelectedEquipID + ".jpeg;" + SelectedEquipID + "*.pdf"
          //"JPEG (" + SelectedEquipID + "*.jpg;" + SelectedEquipID + "*.jpeg;" + SelectedEquipID + "*.pdf)|" + SelectedEquipID + "*.jpg;" + SelectedEquipID + "*.jpeg;" + SelectedEquipID + "*.pdf"
                };
                if (op.ShowDialog() == true)
                {
                    try
                    {
                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        process.StartInfo.FileName = op.FileName;
                        process.Start();
                        process.WaitForExit();
                    }
                    catch
                    {

                    }
                }
            };
            Grid.SetColumn(Certs, 0);
            Grid.SetRow(Certs, DetailsGrid.RowDefinitions.Count - 1);
            DetailsGrid.Children.Add(Certs);
        }
        #endregion


        #region // CalHouse Tab
        private void CalHouse_ComboBox_Fill()
        {
            string sql = "SELECT NAME FROM Contacts";
            // DataTable dataTable = new DataTable();
            // dataTable = SQL.QueryToDataTable(sql);
            // DataView view = new DataView(TempDT);
            DataTable CalHouse_ComboBox = SQL.QueryToDataTable(sql);
            DataRowCollection row = CalHouse_ComboBox.Rows;
            List<string> lst = new List<string>();
            foreach (DataRow dr in CalHouse_ComboBox.Rows)
            {
                var item = dr.ItemArray;
                if (lst.Contains(item[0].ToString()) == false)
                {
                    lst.Add(item[0].ToString());
                }
            }
            CalHouse_CB.ItemsSource = lst;
           
        }
        
        private void CalHouse_CB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ContactsRemoveRows();
            object comboText;
            comboText = CalHouse_CB.SelectedValue;
            string comboTextParsed = String.Empty;
            if(comboText == null)
            {
                CalHouse_CB.SelectedIndex = 0;
                comboText = CalHouse_CB.SelectedIndex; }
            comboTextParsed = CLS.cboParser(comboText.ToString());

            string sql = "SELECT * FROM Contacts WHERE NAME LIKE '" + comboTextParsed + "'";
            DataTable dataTable = new DataTable();
            dataTable = SQL.QueryToDataTable(sql);
            
            // DataRowCollection row = dataTable.Rows;
            List<string> lst = new List<string>();
            int j = 0;
            foreach (DataRow dr in dataTable.Rows)
            {
                foreach(DataColumn dc in dataTable.Columns)
                {
                    Label label = new Label();
                    label.Content = dc.ColumnName.ToString();
                    Grid.SetColumn(label, 1);
                    Grid.SetRow(label, j);
                    TextBox textBox = new TextBox();
                    textBox.Text = dr[dc].ToString();
                    if((label.Content.ToString() == "EMAIL") && (dr[dc].ToString() != " ")) // opens mail client, new message to address specified
                    {
                        string email = string.Format("mailto:{0}", dr[dc].ToString());
                        Hyperlink hyperlink = new Hyperlink()
                        {

                            NavigateUri = new Uri(email)
                        };
                        ContextMenu menu = new ContextMenu();
                        MenuItem menuItem = new MenuItem();
                        menuItem.Header = "Send email";
                        menuItem.Click += delegate {
                            Process.Start(hyperlink.NavigateUri.AbsoluteUri);
                        }; 
                        menu.Items.Add(menuItem);
                        textBox.ContextMenu = menu;

                        ToolTip tip = new ToolTip();
                        tip.Content = string.Format("Right click to send email to {0}", dr[dc].ToString());
                        textBox.ToolTip = tip;
                    }
                    if ((label.Content.ToString() == "WEBSITE") && (dr[dc].ToString() != " ")) // Opens a web browser to address specified
                    {
                        ContextMenu menu = new ContextMenu();
                        MenuItem menuItem = new MenuItem();
                        menuItem.Header = "Visit website";
                        menuItem.Click += delegate {
                            Process.Start(dr[dc].ToString());
                        };
                        menu.Items.Add(menuItem);
                        textBox.ContextMenu = menu;

                        ToolTip tip = new ToolTip();
                        tip.Content = string.Format("Right click to visit {0}", dr[dc].ToString());
                        textBox.ToolTip = tip;
                    }
                    Grid.SetColumn(textBox, 2);
                    Grid.SetRow(textBox, j);
                    j++;
                    CalHouseGrid.Children.Add(textBox);
                    CalHouseGrid.Children.Add(label);
                    if(j > CalHouseGrid.RowDefinitions.Count)
                    {
                        ContactsAddRow(); 
                    }
                }
            }
            ContactsAddRow();
            Button Edit = new Button();
            Edit.Content = "Edit";
            Edit.Height = 25;
            Thickness Edit_Margin = Edit.Margin;
            Edit_Margin.Left = 50;
            Edit.Margin = Edit_Margin;
            Edit.HorizontalAlignment = HorizontalAlignment.Left;
            Grid.SetColumn(Edit, 2);
            Grid.SetRow(Edit, CalHouseGrid.Children.Count - 1);
            if (ReadWriteEnabled == "false")
            {
                Edit.IsEnabled = false;
                ToolTip tip = new ToolTip();
                ToolTipService.SetShowOnDisabled(Edit, true); 
                tip.Content = "Login to enable";
                Edit.ToolTip = tip;
            }
            if (ReadWriteEnabled == "true")
            {
                Edit.IsEnabled = true;
                Edit.ToolTip = null;
            }
            Edit.Click += delegate
            {
                EditCalHouse();
            };
            CalHouseGrid.Children.Add(Edit);

            Button Delete = new Button();
            Delete.Content = "Delete";
            Delete.Height = 25;
            Delete.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetColumn(Delete, 2);
            Grid.SetRow(Delete, CalHouseGrid.Children.Count - 1);
            if (ReadWriteEnabled == "false")
            {
                Delete.IsEnabled = false;
                ToolTip tip = new ToolTip();
                ToolTipService.SetShowOnDisabled(Delete, true);
                tip.Content = "Login to enable";
                Delete.ToolTip = tip;
            }
            if (ReadWriteEnabled == "true")
            {
                Delete.IsEnabled = true;
                Delete.ToolTip = null;
            }
            Delete.Click += delegate
            {
                DeleteCalHouse();
            };
            CalHouseGrid.Children.Add(Delete);

            Button Cancel = new Button();
            Cancel.Content = "Cancel";
            Cancel.Height = 25;
            Thickness Cancel_Margin = Cancel.Margin;
            Cancel_Margin.Right = 50;
            Cancel.Margin = Cancel_Margin;
            Cancel.HorizontalAlignment = HorizontalAlignment.Right;
            Grid.SetColumn(Cancel, 2);
            Grid.SetRow(Cancel, CalHouseGrid.Children.Count - 1);
            Cancel.Click += delegate {
                ContactsRemoveRows();
            };
            CalHouseGrid.Children.Add(Cancel);
        }

        private void ContactsAddRow()
        {
            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(30);
            CalHouseGrid.RowDefinitions.Add(row);
        }

        private void ContactsRemoveRows()
        {
            for (int i = (CalHouseGrid.Children.Count)-1; i >= 1; i-- )
            {


                if ((Grid.GetColumn(CalHouseGrid.Children[i]) == 2) || (Grid.GetColumn(CalHouseGrid.Children[i]) == 1))
                {
                    int index = CalHouseGrid.Children.IndexOf(CalHouseGrid.Children[i]);
                    CalHouseGrid.Children.Remove(CalHouseGrid.Children[index]);
                    
                }
            }
            for (int i = (CalHouseGrid.RowDefinitions.Count); i > 2; i--)
            {
                CalHouseGrid.RowDefinitions.RemoveAt(CalHouseGrid.RowDefinitions.Count - 1);
            }
        }

        private void AddCalHouseBtn()
        {
            CalHouseTab.Focus();
            ContactsRemoveRows();
            DataTable TempTable = new DataTable("TempTable");
            TempTable.Columns.Add("NAME");
            TempTable.Columns.Add("ADDRESS");
            TempTable.Columns.Add("PHONE");
            TempTable.Columns.Add("WEBSITE");
            TempTable.Columns.Add("EMAIL");
            TempTable.Columns.Add("WHO");
            TempTable.Columns.Add("DETAILS");
            DataColumn[] PriKey = new DataColumn[1];
            PriKey[0] = TempTable.Columns["NAME"];
            TempTable.PrimaryKey = PriKey;
            DataRow TempRow = TempTable.NewRow();
            TempRow["NAME"] = "Enter Company Name";
            TempTable.Rows.Add(TempRow);
            int j = 0;
            foreach (DataRow dr in TempTable.Rows)
            {
                foreach (DataColumn dc in TempTable.Columns)
                {
                    Label label = new Label();
                    label.Content = dc.ColumnName.ToString();
                    Grid.SetColumn(label, 1);
                    Grid.SetRow(label, j);
                    TextBox textBox = new TextBox();
                    textBox.Text = dr[dc].ToString();
                    if (label.Content.ToString() == "NAME") // opens mail client, new message to address specified
                    {
                        
                        ToolTip tip = new ToolTip();
                        tip.Content = "The NAME is case sensitive.\nMust be entered as shown on equipment.";
                        textBox.ToolTip = tip;
                    }
                    Grid.SetColumn(textBox, 2);
                    Grid.SetRow(textBox, j);
                    j++;
                    CalHouseGrid.Children.Add(textBox);
                    CalHouseGrid.Children.Add(label);
                    if (j > CalHouseGrid.RowDefinitions.Count)
                    {
                        ContactsAddRow();
                    }
                }
            }
            ContactsAddRow();
            Button Add = new Button();
            Add.Content = "Add New";
            Add.Height = 25;
            Thickness Add_Margin = Add.Margin;
            Add_Margin.Left = 50;
            Add.Margin = Add_Margin;
            Add.HorizontalAlignment = HorizontalAlignment.Left;
            Grid.SetColumn(Add, 2);
            Grid.SetRow(Add, CalHouseGrid.Children.Count-1);
            if (ReadWriteEnabled == "false")
            {
                Add.IsEnabled = false;
                ToolTip tip = new ToolTip();
                ToolTipService.SetShowOnDisabled(Add, true);
                tip.Content = "Login to enable";
                Add.ToolTip = tip;
            }
            if (ReadWriteEnabled == "true")
            {
                Add.IsEnabled = true;
                Add.ToolTip = null;
            }
            Add.Click += delegate
            {
                AddCalHouse();
            };
            CalHouseGrid.Children.Add(Add);
            Button Cancel = new Button();
            Cancel.Content = "Cancel";
            Cancel.Height = 25;
            Thickness Cancel_Margin = Cancel.Margin;
            Cancel_Margin.Right = 50;
            Cancel.Margin = Cancel_Margin;
            Cancel.HorizontalAlignment = HorizontalAlignment.Right;
            Grid.SetColumn(Cancel, 2);
            Grid.SetRow(Cancel, CalHouseGrid.Children.Count - 1);
            Cancel.Click += delegate {
                ContactsRemoveRows();
            };
            CalHouseGrid.Children.Add(Cancel);

        }

        private void AddCalHouse()
        {
            string CalHouseName = "";
            foreach (UIElement i in CalHouseGrid.Children)
            {
                int j = CalHouseGrid.Children.IndexOf(i);
                if (Grid.GetColumn(CalHouseGrid.Children[j]) == 1)
                {
                    string uiElementContent = CLS.cboParser(CalHouseGrid.Children[j].ToString());
                    if (uiElementContent == "NAME")
                    {
                        int row = Grid.GetRow(CalHouseGrid.Children[j]);
                        string uiElementString = CLS.cboParser(CalHouseGrid.Children[CalHouseGrid.Children.IndexOf(FindByCell(CalHouseGrid, row, 2))].ToString());
                        CalHouseName = uiElementString;
                        string sql = string.Format("INSERT INTO Contacts ('" + uiElementContent + "') VALUES ('" + uiElementString + "')");
                        if (uiElementString == "System.Windows.Controls.TextBox") { break; }
                        SQL.ExecuteNonQuery(sql);
                    }
                    else
                    {
                        int row = Grid.GetRow(CalHouseGrid.Children[j]);
                        string uiElementString = CLS.cboParser(CalHouseGrid.Children[CalHouseGrid.Children.IndexOf(FindByCell(CalHouseGrid, row, 2))].ToString());
                        if (uiElementString == "System.Windows.Controls.TextBox") { uiElementString = " "; }
                        string sql = string.Format("UPDATE Contacts SET '" + uiElementContent + "' = '" + uiElementString + "' WHERE NAME LIKE '" + CalHouseName + "'");
                        SQL.ExecuteNonQuery(sql);
                    }
                }
            }
            CalHouse_ComboBox_Fill();
            CalHouseTab.Focus();
            int index = CalHouse_CB.Items.IndexOf(CalHouseName);
            CalHouse_CB.SelectedIndex = index;
            // Popupex popup = new Popupex();
            // TextBlock PopUpText = new TextBlock();
            //     PopUpText.Text = "Add Succesful";
            //     PopUpText.Background = System.Windows.Media.Brushes.LightGreen;
            // PopUpText.T
            // PopUpText.Width = 150;
            // PopUpText.Height = 50;
            // PopUpText.HorizontalAlignment = HorizontalAlignment.Center;
            // PopUpText.VerticalAlignment = VerticalAlignment.Center;
            // popup.Child = PopUpText;
            // popup.VerticalAlignment = VerticalAlignment.Center;
            // popup.HorizontalAlignment = HorizontalAlignment.Center;
            // popup.PlacementTarget = this;
            // popup.Placement = PlacementMode.Center;
            // popup.Width = 150;
            // popup.Height = 50;
            // 
            // 
            // 
            // popup.IsOpen = true;
            

    }

        private void EditCalHouse()
        {
            string CalHouseName = "";
            foreach (UIElement i in CalHouseGrid.Children)
            {
                int j = CalHouseGrid.Children.IndexOf(i);
                if (Grid.GetColumn(CalHouseGrid.Children[j]) == 1)
                {
                    string uiElementContent = CLS.cboParser(CalHouseGrid.Children[j].ToString());
                    if (uiElementContent == "NAME")
                    {
                        int row = Grid.GetRow(CalHouseGrid.Children[j]);
                        string uiElementString = CLS.cboParser(CalHouseGrid.Children[CalHouseGrid.Children.IndexOf(FindByCell(CalHouseGrid, row, 2))].ToString());
                        CalHouseName = uiElementString;
                        // string sql = string.Format("INSERT INTO Contacts ('" + uiElementContent + "') VALUES ('" + uiElementString + "')");
                        // if (uiElementString == "System.Windows.Controls.TextBox") { break; }
                        // SQL.ExecuteNonQuery(sql);
                    }
                    else
                    {
                        int row = Grid.GetRow(CalHouseGrid.Children[j]);
                        string uiElementString = CLS.cboParser(CalHouseGrid.Children[CalHouseGrid.Children.IndexOf(FindByCell(CalHouseGrid, row, 2))].ToString());
                        if (uiElementString == "System.Windows.Controls.TextBox") { uiElementString = " "; }
                        string sql = string.Format("UPDATE Contacts SET '" + uiElementContent + "' = '" + uiElementString + "' WHERE NAME LIKE '" + CalHouseName + "'");
                        SQL.ExecuteNonQuery(sql);
                    }
                }
            }
            CalHouse_ComboBox_Fill();
            CalHouseTab.Focus();
            int index = CalHouse_CB.Items.IndexOf(CalHouseName);
            CalHouse_CB.SelectedIndex = index;
        }

        private void DeleteCalHouse()
        {
            string CalHouseName = "";
            foreach (UIElement i in CalHouseGrid.Children)
            {
                int j = CalHouseGrid.Children.IndexOf(i);
                if (Grid.GetColumn(CalHouseGrid.Children[j]) == 1)
                {
                    string uiElementContent = CLS.cboParser(CalHouseGrid.Children[j].ToString());
                    if (uiElementContent == "NAME")
                    {
                        int row = Grid.GetRow(CalHouseGrid.Children[j]);
                        string uiElementString = CLS.cboParser(CalHouseGrid.Children[CalHouseGrid.Children.IndexOf(FindByCell(CalHouseGrid, row, 2))].ToString());
                        CalHouseName = uiElementString;
                        string sql = string.Format("DELETE FROM Contacts WHERE NAME = '" + CalHouseName + "'");
                        if (uiElementString == "System.Windows.Controls.TextBox") { break; }
                        SQL.ExecuteNonQuery(sql);
                    }
                    // else
                    // {
                    //     int row = Grid.GetRow(CalHouseGrid.Children[j]);
                    //     string uiElementString = CLS.cboParser(CalHouseGrid.Children[CalHouseGrid.Children.IndexOf(FindByCell(CalHouseGrid, row, 2))].ToString());
                    //     if (uiElementString == "System.Windows.Controls.TextBox") { uiElementString = " "; }
                    //     string sql = string.Format("UPDATE Contacts SET '" + uiElementContent + "' = '" + uiElementString + "' WHERE NAME LIKE '" + CalHouseName + "'");
                    //     SQL.ExecuteNonQuery(sql);
                    // }
                }
            }
            CalHouse_ComboBox_Fill();
            CalHouseTab.Focus();
            CalHouse_CB.SelectedIndex = 0;
            


        }

        public class Popupex : Popup
        {
            public Popupex()
            {
                this.Opened += Popupex_Opened;
            }

            void Popupex_Opened(object sender, EventArgs e)
            {
                System.Windows.Threading.DispatcherTimer time = new System.Windows.Threading.DispatcherTimer();
                time.Interval = TimeSpan.FromSeconds(2);
                time.Start();
                time.Tick += delegate
                {
                    this.IsOpen = false;
                    time.Stop();
                };
            }
        }

        UIElement FindByCell(Grid g, int row, int col)
        {
            var childs = g.Children.Cast<UIElement>();
            return childs.Where(x => Grid.GetRow(x) == row && Grid.GetColumn(x) == col).FirstOrDefault();
        }
        #endregion

        #region // ExternalCal Dialog command buttons
        private void Add_Equip_Btn_Click(object sender, RoutedEventArgs e)
        {
            New_External new_External = new New_External(false, null);
            new_External.Owner = App.Current.MainWindow;
            new_External.ShowDialog();
            FillListing();
            Equipment.Focus();

        }

        private void Add_CalHouse_Btn_Click(object sender, RoutedEventArgs e)
        {
            AddCalHouseBtn();
        }

        private void Close_Btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion

    }
}
