﻿using System;
using System.Windows;
using CLS = Classes.IO_class;
using System.Windows.Input;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class GaugeCalc : Window
    {
        public GaugeCalc()
        {
            InitializeComponent();
        }

        private void CalculateBtn_Click(object sender, RoutedEventArgs e)
        {
            string[] GaugeBlocks;
            try
            {
                GaugeBlocks = CLS.readIt("GaugeBlocks.txt").ToArray();


                decimal sum;
                try
                {
                    sum = Convert.ToDecimal(Goal.Text);
                    if (sum > 10.0m)
                    { MessageBox.Show("You must enter a value between .100 and 10.0"); return; }
                    if (sum < .1m)
                    { MessageBox.Show("You must enter a value between .100 and 10.0"); return; }
                }
                catch { MessageBox.Show("You must enter a value between .100 and 10.0"); return; }
                CalculateBtn.Content = "Working...";
                decimal[] decimalGaugeBlocks = Array.ConvertAll(GaugeBlocks, element => decimal.Parse(element));
                CLS.CountSum(decimalGaugeBlocks, sum);
                CalculateBtn.Content = "Calculate";
            }
            catch { MessageBox.Show("\"GuageBlocks.txt\" was not found in executable directory.\n Please create one to use this function."); }
        }
        static private bool focused = false;
        private void Goal_GotFocus(object sender, RoutedEventArgs e)
        {
            if (focused == false) { Goal.Text = ""; }
            focused = true;

        }

        private void Goal_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                CalculateBtn_Click(null,null);
            }
        }
    }
}
