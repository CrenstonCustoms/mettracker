﻿using System.Windows;
using System.Web.UI.DataVisualization.Charting;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for GuageRnR.xaml
    /// </summary>
    public partial class GuageRnR : Window
    {
        public GuageRnR()
        {
            InitializeComponent();
        }

        Chart Chart01 = new Chart();

        public void AnovaCheck()
        {
            Series Series1 = new Series();
            Series Series2 = new Series();
            Series Series3 = new Series();

            AnovaResult result = Chart01.DataManipulator.Statistics.Anova(.05, "Series1,Series2,Series3");
        }
    }
}
