﻿using System.Windows;

using SQL = SQLiteTrans.SQLiteTransaction;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class HistorySelect : Window
    {
        public HistorySelect()
        {
            InitializeComponent();
            
        }

        // no format checking on textboxes. Must be added, that warning is shown when "number of records" is not null, or numeric. ie: a letter or syambol
        // would show warning. Only currently showing all records, regardless of search.
        private void SearchbyID_Click(object sender, RoutedEventArgs e)
        {
            if (SearchByIDText.Text != "")
            {
                try
                {
                    SQLiteTrans.SQLiteTransaction.ConnectToDatabase();
                    string sql = "SELECT * FROM History WHERE Gauge = '" + SearchByIDText.Text + "'";
                    string GaugeNumber;
                    GaugeNumber = SQL.QueryDB(sql, "Gauge");
                    string NumRecordsString = NumRecords.Text;
                    int NumRecord = 0;
                    if (int.TryParse(NumRecordsString, out NumRecord))
                    {}
                    if (AllRecords.IsChecked == true) { NumRecord = 0; }
                    ReportsPrintable reportsPrintable = new ReportsPrintable(GaugeNumber.ToString(), "History", NumRecord);
                    this.Hide();
                    reportsPrintable.Show();
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("The Gauge \"" + SearchByIDText.Text + "\" was not found./nAre you sure you entered it correctly?");
                }

            }
            else
            {
                MessageBox.Show("You must enter a gauge ID to search for.");
            }

        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
