﻿// functions. Needs notice of success or failure. Needs to check existing aspect ration.
// Needs to offer rename to match gauge number 9/25/17

using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using Microsoft.Win32;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for ImageImport.xaml
    /// </summary>
    public partial class ImageImport : Window
    {
        public ImageImport()
        {
            InitializeComponent();
        }

        public void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void btnImport_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog
            {
                Title = "Select an image",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg"
            };
            if (op.ShowDialog() == true)
            {
                byte[] imageBytes = LoadImageData(op.FileName);
                ImageSource imageSource = CreateImage(imageBytes, 360, 270);
                imageBytes = GetEncodedImageData(imageSource, ".jpg");
                
                string CTIsopDirectory;
                object comboText;
                comboText = ImportNumber;
                string ImportNumberParsed = String.Empty;
                ImportNumberParsed = cboParser(comboText.ToString());
                if (ImportNumberParsed == "System.Windows.Controls.TextBox")
                {
                    MessageBox.Show("You must enter an ID for the equipment image to import.\nThis ID is case sensitve.");
                }
                else
                {
                    CTIsopDirectory = @"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + ImportNumberParsed + ".jpg";
                    if
                    (
                        File.Exists(CTIsopDirectory))
                    { MessageBox.Show("This equipment ID already exists.");
                    }
                    else
                    {
                        File.Delete(op.FileName);
                        CTIsopDirectory = @"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + ImportNumberParsed + ".jpg";
                        SaveImageData(imageBytes, CTIsopDirectory);
                        if (MessageBox.Show("Image import successful. \nWould you like to import another?", "Image Imported", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
                        {
                            this.Close();
                            App.Current.Properties["LoadedImage"] = ImportNumberParsed;
                        }
                        else
                        {
                            ImportNumber.Clear();
                        }
                    }
                }
            }
        }

        private static string cboParser(string controlString)
        {
            if (controlString.Contains(':'))
            {
                controlString = controlString.Split(':')[1].TrimStart(' ');
            }
            return controlString;
        }

        private static byte[] LoadImageData(string filePath)

        {
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] imageBytes = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();
            return imageBytes;
        }

        private static void SaveImageData(byte[] imageData, string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(imageData);
            bw.Close();
            fs.Close();
        }

        private static ImageSource CreateImage(byte[] imageData, int decodePixelWidth, int decodePixelHeight)
        {
            if (imageData == null) return null;

            BitmapImage result = new BitmapImage();
            result.BeginInit();
            if (decodePixelWidth > 0)
            {
                result.DecodePixelWidth = decodePixelWidth;
            }

            if (decodePixelHeight > 0)
            {
                result.DecodePixelHeight = decodePixelHeight;
            }

            result.StreamSource = new MemoryStream(imageData);
            result.CreateOptions = BitmapCreateOptions.None;
            result.CacheOption = BitmapCacheOption.Default;
            result.EndInit();
            return result;
        }

        internal byte[] GetEncodedImageData(ImageSource image, string preferredFormat)
        {
            byte[] result = null;
            BitmapEncoder encoder = null;

            switch (preferredFormat.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    encoder = new JpegBitmapEncoder();
                    break;

                case ".bmp":
                    encoder = new BmpBitmapEncoder();
                    break;

                case ".png":
                    encoder = new PngBitmapEncoder();
                    break;

                case ".tif":
                case ".tiff":
                    encoder = new TiffBitmapEncoder();
                    break;

                case ".gif":
                    encoder = new GifBitmapEncoder();
                    break;

                case ".wmp":
                    encoder = new WmpBitmapEncoder();
                    break;
            }

            if (image is BitmapSource)
            {
                MemoryStream stream = new MemoryStream();
                encoder.Frames.Add(BitmapFrame.Create(image as BitmapSource));
                encoder.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                result = new byte[stream.Length];
                BinaryReader br = new BinaryReader(stream);
                br.Read(result, 0, (int)stream.Length);
                br.Close();
                stream.Close();
            }

            return result;
        }
    }
}