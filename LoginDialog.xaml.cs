﻿// Finished 9/21/2017
// Written and tested by Daniel Robbins

using System.Linq;
using System.Windows;
using System.Windows.Input;
using SQL = SQLiteTrans.SQLiteTransaction;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for LoginDialog.xaml
    /// </summary>
    public partial class LoginDialog : Window
    {
        public LoginDialog()
        {
            InitializeComponent();
        }

        void CheckPassword()
        {
            if (Passwordtext.Password == "mEtrology")
            {
                App.Current.Properties["ReadWriteAccess"] = "true";
                var window2 = Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) as MainWindow;
                window2.Settings.IsEnabled = true;
                if (App.Current.Properties["ReadOnly"].ToString() == "RO_True")
                {
                    MessageBox.Show("Your password is correct but you have \nread-only access to the database file on the network.");
                    SQL.ReadOnlyPermission();
                    App.Current.Properties["ReadWriteAccess"] = "false";
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("The password you entered was incorrect. Please try again.");
            }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            CheckPassword();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                CheckPassword();
            }
        }
    }
}