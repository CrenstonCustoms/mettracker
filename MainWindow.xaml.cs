﻿using System.Windows;
using System.Data.SQLite;
using System.IO;
using System.Diagnostics;
using System;
using CLS = Classes.IO_class;
using System.Data;
using Microsoft.Win32;
using SQL = SQLiteTrans.SQLiteTransaction;

// BUG: Documents menu crashes when file not found
// BUG: GaugeBlock calculator crashes when txt file not found

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string TodayDateString;
        string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
        DataTable ExternalDue = new DataTable();

        public MainWindow()
        {
            App.Current.Properties["ReadWriteAccess"] = "false";

            App.Current.Properties["ReadOnly"] = "RO_False";

            InitializeComponent();
            DBAccess();

            this.Title = "MetTracker - Database Read Only";

            Settings.IsEnabled = false;
        }

        private void DBAccess()
        {
            // File does have access or readonly? Sets App.Current.Properties["ReadOnly"] to true or false.
            try
            {
                FileInfo fi = new FileInfo(DBDirectory);
                bool IsReadOnly = fi.IsReadOnly;  //only true if read only
                long FileLength = fi.Length;   //fails if file not found

                if (IsReadOnly == true)
                {
                    MessageBox.Show("The database is open as read only.\nContact your network administrator for write access if needed.");

                    App.Current.Properties["ReadOnly"] = "RO_True";
                    this.Title = "MetTracker      Database READ ONLY";
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to the Calibration Records Database.\n" +
                    "Contact your network administrator for assistance.");
                System.Windows.Application.Current.Shutdown();
                this.Close();
                Environment.Exit(0);
            }

            catch (Exception)
            {
                MessageBoxResult result = MessageBox.Show("The database was not found\nWould you like to specify another?", "DB not Found", MessageBoxButton.YesNo);

                switch (result)
                {
                    case MessageBoxResult.Yes:

                        OpenFileDialog op = new OpenFileDialog
                        {
                            Title = "Select the database",
                            Filter = "SQLite Database (SOP7_6.db)|SOP7_6.db"
                        };

                        if (op.ShowDialog() == true)
                        {
                            MetTracker.Properties.Settings.Default.DBDirectory = op.FileName.ToString();
                            DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
                        }
                        DBAccess();
                        return;
                    case MessageBoxResult.No:
                        Environment.Exit(0);
                        break;
                }
            }

            if (App.Current.Properties["ReadOnly"].ToString() == "RO_False")
            {
                TodaysStatus();
            }
        }

        // Populate "dashboard" on main screen.
        private void TodaysStatus()
        {
            string ItemsDue = "0";
            DateTime Today = new DateTime();
            Today = DateTime.Today;
            int MonthString = int.Parse(Today.ToString("MM"));
            string Month_string = MonthString.ToString();
            string Month_stringLong = Month_string;
            if(Month_string.Length == 1)
            {
                Month_stringLong = "0" + Month_string;
            }
            string TodayDate = "\"" + MonthString.ToString() + "/" + @"%" + Today.ToString("yyyy") + "\" OR Due LIKE \"" + Month_stringLong + "/" + @"%" + Today.ToString("yyyy") + "\"";
        

        SQLiteConnection m_dbConnection;

            string DBDirectory = CLS.GetSettingsStrings(@"DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();

            DataTable dt = new DataTable();

            string sql = "SELECT Gauge FROM Equipment WHERE Due LIKE " + TodayDate + "";
            TodayDateString = TodayDate;

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
            sda.Fill(dt);

            ItemsDue = dt.Rows.Count.ToString();

            m_dbConnection.Close();

            ItemsStatusTextBlock.Text = "You have " + ItemsDue + " calibrations\ndue this month.";

            // double days = double.Parse(CLS.GetSettingsStrings("ExternalCalInterval"));
            double days = CLS.Settings.ExternalCalInterval;
            DataTable tempData = new DataTable();
            DateTime FutureDate = new DateTime();
            FutureDate = DateTime.Now;
            FutureDate = FutureDate.AddDays(days);
            string FutureDateString = FutureDate.ToShortDateString();
            string sql1 = "SELECT * FROM External";
            tempData = SQL.QueryToDataTable(sql1);
            ExternalDue = tempData.Clone();

            int ItemsInRange = 0;

            foreach (DataColumn dc in tempData.Columns)
            {
                foreach (DataRow dr in tempData.Rows)
                {
                    if (dc.ColumnName == "DUE")
                    {
                        string tempString = dr[dc].ToString();
                        if (tempString == "") { continue; }

                        DateTime oDate = DateTime.ParseExact(dr[dc].ToString(), "M/dd/yyyy", null);

                        if ((oDate >= DateTime.Today) && (oDate <= FutureDate))
                        { ItemsInRange = ++ItemsInRange; ExternalDue.Rows.Add(dr.ItemArray); }

                        oDate = DateTime.Now;

                    }
                }
            }

            ExternalStatusTextBlock.Text = "You have " + ItemsInRange + " externally\ncalibrated items due\nin the next " + days + " days.";
        }

        // Login dialog to open password prompt for read/write access, only if "ReadOnly" is False.
        private void MenuLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginDialog loginDialog = new LoginDialog();
            loginDialog.Owner = this;
            loginDialog.ShowDialog();
            if (App.Current.Properties["ReadWriteAccess"].ToString() == "true")
            { this.Title = "MetTracker - Logged In"; }
        }

        // Dialog to view and update calibration information on entered equipment
        private void MenuSearchbyID_Click(object sender, RoutedEventArgs e)
        {
            SearchByID searchByID = new SearchByID();
            searchByID.Owner = this;
            searchByID.ShowDialog();
        }

        // Dialog to view printable reports based on available search criteria.
        private void Choose_Click(object sender, RoutedEventArgs e)
        {
            Reports reports = new Reports();
            reports.Owner = this;
            reports.ShowDialog();
        }

        // Closes application
        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            System.Windows.Application.Current.Shutdown();
        }

        // Dialog to add new equipment to records
        private void MenuNewItem_Click(object sender, RoutedEventArgs e)
        {
            AddEquipmentWindow addEquipmentWindow = new AddEquipmentWindow();
            addEquipmentWindow.Owner = this;
            addEquipmentWindow.ShowDialog();
        }

        // Dialog to browse all equipment in records

        public void MenuBrowse_Click(object sender, RoutedEventArgs e)
        {
            ReportsPrintable reportsPrintable = new ReportsPrintable(null, null);
            reportsPrintable.Owner = this;
            reportsPrintable.ShowDialog();
        }

        // Shortcut past search by ID dialog
        private void MenuUpdate_Click(object sender, RoutedEventArgs e)
        {
            EquipmentWindow equipmentWindow = new EquipmentWindow("EnterID");
            equipmentWindow.Owner = this;
            equipmentWindow.ShowDialog();
        }

        // Opens dialog to name a piece of equipment, and browse the file system to import an image as named.
        private void MenuImportImage_Click(object sender, RoutedEventArgs e)
        {
            ImageImport imageImport = new ImageImport();
            imageImport.Owner = this;
            imageImport.ShowDialog();
        }

        // View historical records of past transactions.
        private void MenuHistory_Click(object sender, RoutedEventArgs e)
        {
            HistorySelect historySelect = new HistorySelect();
            historySelect.Owner = this;
            historySelect.ShowDialog();
        }

        // View all vendors, and the equipment currently issued to them.
        private void MenuVendors_Click(object sender, RoutedEventArgs e)
        {
            ReportsPrintable reportsPrintable = new ReportsPrintable(null, "Vendors");
            reportsPrintable.Owner = this;
            reportsPrintable.ShowDialog();
        }

        // Opens basic dialog to calculate Gauge Blocks needed to stack up to given sum.
        private void MenuGaugeCalc_Click(object sender, RoutedEventArgs e)
        {
            GaugeCalc gaugeCalc = new GaugeCalc();
            gaugeCalc.Owner = this;
            gaugeCalc.ShowDialog();
        }

        // Opens File Explorer to specified directory.
        private void SOP_Click(object sender, RoutedEventArgs e)
        {
            string SOPDirectory = CLS.GetSettingsStrings("SOPDirectory").ToString();
            try
            {
                var fi = new FileInfo(SOPDirectory);
                // var FileLength = fi.Length;   //fails if file not found
                using (var process = new Process())
                {
                    process.StartInfo.UseShellExecute = true;
                    process.StartInfo.FileName = SOPDirectory;
                    process.Start();
                }
            }
            catch
            {
                MessageBox.Show(string.Format("The \"{0}\" folder could not be found.", SOPDirectory));
            }
        }

        // Opens File Explorer to specified directory.
        private void WI_Ref_Click(object sender, RoutedEventArgs e)
        {
            string WIDirectory = CLS.GetSettingsStrings("WIDirectory").ToString();
            try
            {
                var fi = new FileInfo(WIDirectory);
                // var FileLength = fi.Length;   //fails if file not found
                using (var process = new Process())
                {
                    process.StartInfo.UseShellExecute = true;
                    process.StartInfo.FileName = WIDirectory;
                    process.Start();
                }
            }
            catch
            {
                MessageBox.Show(string.Format("The \"{0}\" folder could not be found.", WIDirectory));
            }
        }

        // Opens File Explorer to specified directory.
        private void CalInstr_Click(object sender, RoutedEventArgs e)
        {
            string SOPCalDirectory = CLS.GetSettingsStrings("SOPCalDirectory").ToString();
            try
            {
                var fi = new FileInfo(SOPCalDirectory);
                // var FileLength = fi.Length;   //fails if file not found
                using (var process = new Process())
                {
                    process.StartInfo.UseShellExecute = true;
                    process.StartInfo.FileName = SOPCalDirectory;
                    process.Start();
                }
            }
            catch
            {
                MessageBox.Show(string.Format("The \"{0}\" folder could not be found.", SOPCalDirectory));
            }
        }

        private void GRnR_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("The Gauge RnR is not yet functional.");
        }

        private void AGRnR_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("The Attribute Gauge RnR is not yet functional.");
        }

        private void RnRReports_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("The Gauge RnR Reports is not yet functional.");
        }

        private void Support_Click(object sender, RoutedEventArgs e)
        {
            Support support = new Support();
            support.Owner = this;
            support.ShowDialog();
        }

        private void HowTo_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("The How-To Documentation is not yet functional.");
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MetTracker.About about = new MetTracker.About();
            about.Owner = this;
            about.ShowDialog();
        }

        // Allows selection of *.db file, WI, SOP, and SOP(Calibration instructions) directories
        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings();
            settings.Owner = this;
            settings.ShowDialog();
        }

        // Opens File Explorer to specified directory.
        private void DWGSearch_Click(object sender, RoutedEventArgs e)
        {
            string DWGDirectory = MetTracker.Properties.Settings.Default.DWGDirectory.ToString();

            if (CLS.DirectoryAccessCheck(DWGDirectory) == false)
            {
                Process process = new Process();
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.FileName = CLS.GetSettingsStrings("DWGDirectory") + "\\" + CLS.GetSettingsStrings("DWGTable");
                process.Start();
            }
            else
            {
                MessageBox.Show("You do not have access to {0}.\nContact your network administrator for access.", DWGDirectory.ToString());
            }
        }

        // Opens the printable reports dialog to show the items due this month.
        private void CurrentDueBtn_Click(object sender, RoutedEventArgs e)
        {
            ReportsPrintable reportsPrintable = new ReportsPrintable(TodayDateString, "Date_Internal");
            reportsPrintable.Owner = this; 
            reportsPrintable.ShowDialog();
            TodaysStatus();
        }

        private void MenuExternal_Click(object sender, RoutedEventArgs e)
        {
            ExternalCal externalCal = new ExternalCal(null);
            externalCal.Owner = this;
            externalCal.ShowDialog();
        }

        private void ExternalBtn_Click(object sender, RoutedEventArgs e)
        {
            ExternalCal externalCal = new ExternalCal(ExternalDue);
            externalCal.Owner = this;
            externalCal.ShowDialog();
            TodaysStatus();
        }

        /*
         * BUG**** Image import directly from camera external storage crashes program. Import from local folder works fine. 12/19/2017
         */
    }
}