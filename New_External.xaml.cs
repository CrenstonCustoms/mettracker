﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CLS = Classes.IO_class;
using SQL = SQLiteTrans.SQLiteTransaction;
using System.Text.RegularExpressions;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for New_External.xaml
    /// </summary>
	/* 
	* login read/write check needed
	*/
    public partial class New_External : Window
    {
        string[] IndData;
        bool Edit_True = false;
        string EDIT_ID;

        public New_External(bool edit_equip, string ID)
        {
            InitializeComponent();
            if(edit_equip == true)
            {
                EDIT_ID = ID;
                Edit_True = true;
                this.Title = string.Format("Edit {0}", ID);
                string sql = string.Format("SELECT * FROM External WHERE ID = '" + ID + "'");
                dataTable = SQL.QueryToDataTable(sql);
                DataRow row = dataTable.Select(string.Format("ID = '" + ID + "'"))[0];
                int ID_Index = dataTable.Rows.IndexOf(row);
                ID_TB.Text = ID;
                DEPARTMENT_TB.Text = dataTable.Rows[ID_Index].Field<string>("DEPARTMENT");
                CALHOUSE_TB.Text = dataTable.Rows[ID_Index].Field<string>("CAL_HOUSE");
                INSP_TB.Text = dataTable.Rows[ID_Index].Field<string>("INSP");
                DUE_TB.Text = dataTable.Rows[ID_Index].Field<string>("DUE");
                TYPE_TB.Text = dataTable.Rows[ID_Index].Field<string>("TYPE");
                CALINT_TB.Text = dataTable.Rows[ID_Index].Field<string>("CAL_INT");
                DETAILS_TB.Text = dataTable.Rows[ID_Index].Field<string>("DETAILS");

                string SpecsData = "";
                SpecsData = dataTable.Rows[ID_Index].Field<string>("SPECS");
                // SpecsGrid.Children.Clear();
                if ((SpecsData == null) || (SpecsData == " ")) // checks for empty string
                {
                    return;
                }
                
                IndData = Regex.Split(SpecsData, ";");
                Add_BTN.Content = "Edit";
                FillDetailsGrid(ID);
            }
        }

        private void FillDetailsGrid(string ID)
        {
            int IndDataLength = IndData.Length;
            if (IndData.Length == 0) { return; }
            for (int i = 0; i < IndDataLength; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Name = string.Format("row{0}", i);
                row.Height = new GridLength(25);
                SpecsGrid.RowDefinitions.Add(row);
            }

            int j = 1;
            foreach (string value in IndData)
            {
                if (value == "") { return; }
                string[] SplitData = Regex.Split(value, ":");
                if (SplitData.Length < 2) {return; }
                Label label = new Label();
                label.Content = SplitData[0];
                Grid.SetColumn(label, 0);
                Grid.SetRow(label, j);
                TextBox textBox = new TextBox();
                textBox.Text = SplitData[1];
                Grid.SetColumn(textBox, 1);
                Grid.SetRow(textBox, j);
                SpecsGrid.Children.Add(label);
                SpecsGrid.Children.Add(textBox);
                j++;
            }
        }

        DataTable dataTable = new DataTable();

        private void AddSpec_BTN_Click(object sender, RoutedEventArgs e)
        {
            TextBox textBoxLabel = new TextBox();
            TextBox textBoxValue = new TextBox();
            textBoxLabel.Height = 25;
            textBoxValue.Height = 25;
            textBoxLabel.Text = "Enter new spec";
            textBoxLabel.GotFocus += delegate
            { textBoxLabel.Text = ""; };
            textBoxValue.GotFocus += delegate
            { textBoxValue.Text = ""; };
            textBoxValue.Text = "Enter value";
            RowDefinition rowDefinition = new RowDefinition();
            rowDefinition.Height = new GridLength(30);
            SpecsGrid.RowDefinitions.Add(rowDefinition);
            Grid.SetColumn(textBoxLabel, 0);
            Grid.SetColumn(textBoxValue, 1);
            Grid.SetRow(textBoxLabel, SpecsGrid.RowDefinitions.Count - 1);
            Grid.SetRow(textBoxValue, SpecsGrid.RowDefinitions.Count - 1);
            SpecsGrid.Children.Add(textBoxLabel);
            SpecsGrid.Children.Add(textBoxValue);
        }

        private void SpecsRemoveRows()
        {
            for (int i = (SpecsGrid.Children.Count) - 1; i >= 1; i--)
            {


                if (Grid.GetRow(SpecsGrid.Children[i]) != 0)
                {
                    int index = SpecsGrid.Children.IndexOf(SpecsGrid.Children[i]);
                    SpecsGrid.Children.Remove(SpecsGrid.Children[index]);

                }
            }
            for (int i = (SpecsGrid.RowDefinitions.Count); i > 1; i--)
            {
                SpecsGrid.RowDefinitions.RemoveAt(SpecsGrid.RowDefinitions.Count - 1);
            }

            ID_TB.Text = "";
            DEPARTMENT_TB.Text = "";
            CALHOUSE_TB.Text = "";
            INSP_TB.Text = "";
            DUE_TB.Text = "";
            TYPE_TB.Text = "";
            CALINT_TB.Text = "";
            DETAILS_TB.Text = "";
        }

        UIElement FindByCell(Grid g, int row, int col)
        {
            var childs = g.Children.Cast<UIElement>();
            return childs.Where(x => Grid.GetRow(x) == row && Grid.GetColumn(x) == col).FirstOrDefault();
        }

        private void Close_BTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Add_BTN_Click(object sender, RoutedEventArgs e)
        {


            string SpecsString = " ";
            foreach (RowDefinition i in SpecsGrid.RowDefinitions)
            {
                if ((SpecsGrid.RowDefinitions.IndexOf(i) == 0) || (SpecsGrid.RowDefinitions.IndexOf(i) == 0))
                {
                    continue;
                }
                string Label = string.Format(CLS.cboParser(SpecsGrid.Children[SpecsGrid.Children.IndexOf(FindByCell(SpecsGrid, SpecsGrid.RowDefinitions.IndexOf(i), 0))].ToString()) + ":");
                string Value = string.Format(CLS.cboParser(SpecsGrid.Children[SpecsGrid.Children.IndexOf(FindByCell(SpecsGrid, SpecsGrid.RowDefinitions.IndexOf(i), 1))].ToString()) + "");
                if (SpecsString == " ")
                {
                    SpecsString = Label + Value;
                    continue;
                }
                if (SpecsString != " ")
                {
                    SpecsString = SpecsString + ";" + Label + Value;
                }
            }
            if (Edit_True == true)
            {
                string sql1 = string.Format("UPDATE External SET ID = '" + ID_TB.Text + "', DEPARTMENT = '" + DEPARTMENT_TB.Text + "', CAL_HOUSE = '" +
                    CALHOUSE_TB.Text + "', INSP = '" + INSP_TB.Text + "', DUE = '" + DUE_TB.Text + "', TYPE = '" + TYPE_TB.Text + "', CAL_INT = '" +
                    CALINT_TB.Text + "', DETAILS = '" + DETAILS_TB.Text + "', SPECS = '" + SpecsString + "' WHERE ID = '" + EDIT_ID + "'");
                SQL.ExecuteNonQuery(sql1);
                MessageBox.Show(string.Format("{0} edit successful", EDIT_ID));
                this.Close();
            }

            if (Edit_True == false)
            {
                string sql = string.Format("INSERT INTO External (ID, DEPARTMENT, CAL_HOUSE, INSP, DUE, TYPE, CAL_INT, DETAILS, SPECS) VALUES ('" + ID_TB.Text + "','" + DEPARTMENT_TB.Text
                    + "','" + CALHOUSE_TB.Text + "','" + INSP_TB.Text + "','" + DUE_TB.Text + "','" + TYPE_TB.Text + "','" + CALINT_TB.Text + "','" + DETAILS_TB.Text + "','" +
                    SpecsString + "')");
                SQL.ExecuteNonQuery(sql);
                SpecsRemoveRows();
                MessageBox.Show(string.Format("{0} was added", ID_TB.Text));
                
            }
        }

        private void Clear_BTN_Click(object sender, RoutedEventArgs e)
        {
            SpecsRemoveRows();
        }
    }
}
