﻿using System;
using System.Windows;
using System.Data.SQLite;
using System.Data;
using CLS = Classes.IO_class;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        SQLiteConnection m_dbConnection;
        void connectToDatabase()
        {
            // m_dbConnection = new SQLiteConnection("Data Source=SOP7_6.db;Version=3;");
            // m_dbConnection.Open();
            string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
        }
        
        void FillDataGrid()
        {

            connectToDatabase();

            string sql = "SELECT * FROM Equipment";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //SQLiteDataReader reader = command.ExecuteReader();
            SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            MainDataGrid.ItemsSource = dt.DefaultView;

        }

        public ReportWindow()
        {
            
            InitializeComponent();
            FillDataGrid();




            // your method to pull data from database to datatable   

        }
    }
}
