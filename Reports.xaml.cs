﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data.SQLite;
using CLS = Classes.IO_class;
using SQL = SQLiteTrans.SQLiteTransaction;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for Reports.xaml
    /// </summary>
    public partial class Reports : Window
    {
        string query;

        public Reports()
        {
            InitializeComponent();
            QueryInternalDatabase();
        }
        #region internal
        #region Date Picker
        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var picker = sender as DatePicker;

            DateTime? date = picker.SelectedDate;

            if (date == null)
            {
                // ... A null object.
                MessageBox.Show("No Date Selected");
            }
            else
            {
                // ... No need to display the time.
                // MessageBox.Show(string.Format("{0}", date.Value.ToShortDateString()));
            }
        }
        #endregion
        #region Date Click
        private void ReportQueryButtonDATE_Click(object sender, RoutedEventArgs e)
        {
            if (DatePicked.SelectedDate == null)
            {
                // ... A null object.
                MessageBox.Show("No Date Selected");
            }
            else
            {
                // ... No need to display the time.
                // MessageBox.Show(string.Format("You searched for equipment due on {0}", DatePicked.SelectedDate.Value.ToShortDateString()));

                string DateString = DatePicked.SelectedDate.Value.ToShortDateString();
                char[] delimiterChars = { '/' };
                string[] DateSplit = DateString.Split(delimiterChars);
                string MonthString = DateSplit[0];
                string MonthStringLong = MonthString;
                int MonthStringLength = MonthString.Length;
                if(MonthStringLength == 1)
                    {
                    MonthStringLong = "0" + MonthString;
                }
                string YearString;
                string DayString = "1";
                if (DateSplit[2] == null){ YearString = DateSplit[1]; }
                else{ DayString = DateSplit[1]; YearString = DateSplit[2]; }
                
                
                string SearchString = "";
                if (Internal_Report.IsChecked == true) { SearchString = "\"" + MonthString + "/" + @"%" + YearString + "\" OR Due LIKE \"" + MonthStringLong + "/" + @"%" + YearString + "\""; }
                if (External_Report.IsChecked == true) { SearchString = "\"" + MonthString + "/" + @"%" + YearString + "\" OR Due LIKE \"" + MonthStringLong + "/" + @"%" + YearString + "\""; }
                if (Internal_Report.IsChecked == true) { query = "Date_Internal"; }
                if (External_Report.IsChecked == true) { query = "Date_External"; }
                ReportsPrintable reportsPrintable = new ReportsPrintable(SearchString, query)
                { Owner = App.Current.MainWindow };
                reportsPrintable.ShowDialog();
            }
        }
        #endregion
        //works
        void ReportQueryButtonDEPARTMENT_Click(object sender, RoutedEventArgs e)
        {
            if (Internal_Report.IsChecked == true) { query = "Department_Internal"; }
            if (External_Report.IsChecked == true) { query = "Department_External"; }
            object comboText;
            comboText = Department_ComboBox.SelectedValue;
            string comboTextParsed = String.Empty;
            comboTextParsed = CLS.cboParser(comboText.ToString());
            // MessageBox.Show(string.Format("You searched for equipment in the {0} department", comboTextParsed)); // only used for debug
            ReportsPrintable reportsPrintable = new ReportsPrintable(comboTextParsed, query)
            { Owner = App.Current.MainWindow };
            reportsPrintable.ShowDialog();
        }
        
        // works
        private void ReportQueryButtonTYPE_Click(object sender, RoutedEventArgs e)
        {
            if (Internal_Report.IsChecked == true) { query = "Type_Internal"; }
            if (External_Report.IsChecked == true) { query = "Type_External"; }
            
            object comboText;
            comboText = Type_ComboBox.SelectedItem;
            string comboTextParsed = String.Empty;
            comboTextParsed = CLS.cboParser(comboText.ToString());
            string comboTextParsed1 = comboTextParsed.Replace(@"\\", @""); // strips '\' character from string
            // MessageBox.Show(string.Format("You searched for equipment of type: {0}", comboTextParsed1)); // only used for debug
            // comboTextParsed =  + comboTextParsed
            ReportsPrintable reportsPrintable = new ReportsPrintable(comboTextParsed1, query)
            { Owner = App.Current.MainWindow };
            reportsPrintable.ShowDialog();
        }


        SQLiteConnection m_dbConnection;

        void connectToDatabase()
        {

            string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
        }
        
        public void QueryInternalDatabase()
        {
            // connectToDatabase();
            string sql_1 = "SELECT DISTINCT Location FROM Equipment";
            // SQLiteCommand command = new SQLiteCommand(sql_1, m_dbConnection);
            // using (SQLiteDataReader reader = command.ExecuteReader())
            // {
            //     List<string> Departments = new List<string>();
            //     while (reader.Read())
            //     {
            //         Departments.Add(Convert.ToString(reader["Location"]));
            //     }
            //     Departments.Sort();
            //     Department_ComboBox.ItemsSource = Departments;
            // }
            List<string> Departments = SQL.QueryToListString(sql_1, "Location");
            Departments.Sort();
            Department_ComboBox.ItemsSource = Departments;


            // Type[] type = null;
            sql_1 = "SELECT DISTINCT Desc FROM Equipment";
            List<string> Types = SQL.QueryToListString(sql_1, "Desc");
            Types.Sort();
            Type_ComboBox.ItemsSource = Types;

            // SQLiteCommand command2 = new SQLiteCommand(sql_1, m_dbConnection);
            // using (SQLiteDataReader reader = command2.ExecuteReader())
            // {
            //     List<string> Types = new List<string>();
            //     while (reader.Read())
            //     {
            //         Types.Add(Convert.ToString(reader["Desc"]));
            //     }
            //     Types.Sort();
            //     Type_ComboBox.ItemsSource = Types;
            // }


        }
        #endregion

        public void QueryExternalDatabase()
        {
            string sql = "SELECT DISTINCT DEPARTMENT FROM External";
            List<string> Departments = SQL.QueryToListString(sql, "DEPARTMENT");
            Departments.Sort();
            Department_ComboBox.ItemsSource = Departments;

            sql = "SELECT DISTINCT TYPE FROM External";
            List<string> Types = SQL.QueryToListString(sql, "TYPE");
            Types.Sort();
            Type_ComboBox.ItemsSource = Types;
        }

        private void External_Report_Click(object sender, RoutedEventArgs e)
        {
            QueryExternalDatabase();
        }

        private void Internal_Report_Click(object sender, RoutedEventArgs e)
        {
            QueryInternalDatabase();
        }
    }
}