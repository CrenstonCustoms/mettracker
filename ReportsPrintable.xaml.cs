﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Data;
using System.Data.SQLite;
using SUT.PrintEngine.Utils;
using XamlWriter = System.Windows.Markup.XamlWriter;
using CLS = Classes.IO_class;
using System.Windows.Controls;

// Suggestion: Overwrite db column names for better appearance

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for Reports.xaml
    /// </summary>
    public partial class ReportsPrintable : Window
    {
        public ReportsPrintable(String SearchItem1, String query, int History = 0)
        {
            InitializeComponent();
            FillDataGrid(SearchItem1, query, History);
        }

        System.Threading.CancellationTokenSource cts;

        public string SearchQuery = "";
        SQLiteConnection m_dbConnection;

        bool ExternalReport = false;

        void connectToDatabase()
        {
            string DBDirectory = CLS.GetSettingsStrings(@"DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
        }

        DataTable dt = new DataTable();

        public void QueryDatabase(string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
            sda.Fill(dt);
            MainDataGrid.ItemsSource = dt.DefaultView;

            string ItemsInDataTable = dt.Rows.Count.ToString();
            this.Title = "Reports   " + ItemsInDataTable + " items";
        }

        void FillDataGrid(string SearchItem, string query, int History = 0)
        {
            connectToDatabase();

            string sql = "SELECT * FROM Equipment";

            if (query == "Department_Internal")
            {
                //works
                sql = "SELECT Gauge, Owner, Location, Desc, Due FROM Equipment WHERE Location = '" + SearchItem + "'";
            }
            if (query == "Department_External")
            {
                ExternalReport = true;
                sql = "SELECT ID, DEPARTMENT, CAL_HOUSE, DUE, TYPE, SERIAL FROM External WHERE DEPARTMENT = '" + SearchItem + "'";
            }

            if (query == "Date_Internal")
            {
                sql = "SELECT Gauge, Owner, Location, Desc, Due FROM Equipment WHERE Due LIKE " + SearchItem + "";
            }
            if (query == "Date_External")
            {
                ExternalReport = true;
                sql = "SELECT ID, DEPARTMENT, CAL_HOUSE, DUE, TYPE, SERIAL FROM External WHERE DUE LIKE " + SearchItem + "";
            }

            if (query == "Type_Internal")
            {
                sql = "SELECT Gauge, Owner, Location, Desc, Due FROM Equipment WHERE Desc = '" + SearchItem + "'";
            }
            if (query == "Type_External")
            {
                ExternalReport = true;
                sql = "SELECT ID, DEPARTMENT, CAL_HOUSE, DUE, TYPE, SERIAL FROM External WHERE TYPE = '" + SearchItem + "'";
            }

            if (query == "History")
            {
                sql = "Select * FROM History WHERE Gauge = '" + SearchItem + "' ORDER BY Key Desc";
                if (History != 0)
                {
                    sql = " SELECT * FROM History WHERE Gauge = '" + SearchItem + "' ORDER BY Key DESC LIMIT " + History + "";
                }
            }

            if (query == "Vendors")
            {
                sql = "SELECT * FROM Vendors";
                SearchQuery = "Vendors";
            }

            QueryDatabase(sql);
        }

        ContextMenu cm = new ContextMenu();

        void MainDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string tooltip = null;

            ;
            MenuItem mi = new MenuItem();
            mi.Header = null;

            

            switch (e.Column.Header.ToString())
            {
                case "Gauge":
                    e.Column.Width = 80;
                    if (SearchQuery == "") { tooltip = "Double click Gauge ID to view its details."; }
                    if (SearchQuery == "Vendors")
                    {
                        tooltip = "Double click Gauge ID to view its details,\nor right click to remove from Vendor Equipment Tracking.";
                        e.Column.Width = 80;
                        mi.Header = "Remove Gauge from Vendor Listing";
                        mi.Click += new RoutedEventHandler(Right_Click);
                    }

                    break;

                case "Location":
                    Style style = new Style(typeof(DataGridCell));
                    style.Setters.Add(new Setter(DataGridCell.BackgroundProperty, new Binding(e.PropertyName) { Converter = new ValueToColorConverter() }));
                    e.Column.CellStyle = style;
                    break;

                case "Due":
                    e.Column.Width = 120;
                    e.Column.Header = "Due Date";
                    break;

                case "Insp":
                    e.Column.Width = 120;
                    e.Column.Header = "Inspected Date";
                    break;

                case "Desc":
                    e.Column.Header = "Description";
                    break;

                case "DevAsRec":
                    e.Column.Header = "Deviation As Received";
                    break;

                case "Adj":
                    e.Column.Header = "Adjusted";
                    break;

                case "DevAsRet":
                    e.Column.Header = "Deviation As Returned";
                    break;

                case "CAL_HOUSE":
                    e.Column.Header = "Calibration House";
                    break;

                case "DUE":
                    e.Column.Header = "Due Date";
                    break;

                case "INSP":
                    e.Column.Header = "Inspected Date";
                    break;

                case "CAL_INT":
                    e.Column.Header = "Calibration Frequency";
                    break;

                case "CalibrationFreq":
                    e.Column.Header = "Calibration Frequency";
                    break;

                case "CalFreq":
                    e.Column.Header = "Calibration Frequency";
                    break;
            }

            if (tooltip != null)
            {
                cm.Items.Add(mi);
                var style = new Style(typeof(DataGridCell));
                style.Setters.Add(new Setter(ToolTipService.ToolTipProperty, tooltip));
                if (SearchQuery == "Vendors") { style.Setters.Add(new Setter(ContextMenuService.ContextMenuProperty, cm)); }
                // style.Setters.Add(new EventSetter(MouseEnterEvent,
                //    new MouseEventHandler(DataGridCell_MouseEnter)));
                // style.TargetType = typeof(DataGridCell);
                // Trigger t = new Trigger();
                // t.Property = DataGridCell.IsMouseOverProperty;
                // t.Value = true;
                // DataGridCell dgc = new DataGridCell();
                // Setter setter = new Setter();
                // setter.Property = dgc.AddHandler(DataGridCell.MouseEnterEvent, new RoutedEventHandler(DataGridCell_MouseEnter));
                
                e.Column.CellStyle = style;
            }
        }

        private void Right_Click(object sender, RoutedEventArgs e)
        {
            if (SearchQuery != "Vendors") return;

            //Get the clicked MenuItem
            var menuItem = (MenuItem)sender;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (DataGridCell)contextMenu.PlacementTarget;

            string ID;
            ID = cboParser(item.ToString()); // parse extra data from string
            MessageBox.Show(string.Format("Gauge {0} has been removed from the Vendor Tracking.", ID));
            string sql = "DELETE FROM Vendors WHERE Gauge = '" + ID + "'";
            connectToDatabase();
            SQLiteCommand commandDelete = new SQLiteCommand(sql, m_dbConnection);
            commandDelete.ExecuteNonQuery();
            m_dbConnection.Close();

            dt.Clear();
            FillDataGrid(null, "Vendors");
        }

        // prevents user from editing datagrid cell contents. a form of readonly protection
        private void MainDataGrid_CellEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (this.MainDataGrid.SelectedItem != null)
            {
                (sender as DataGrid).RowEditEnding -= MainDataGrid_CellEditEnding;
                (sender as DataGrid).CancelEdit();
                (sender as DataGrid).Items.Refresh();
                (sender as DataGrid).RowEditEnding += MainDataGrid_CellEditEnding;
            }
        }

        // Works with "ByDepartment". no others tested.
        void MenuPrint_Click(object sender, RoutedEventArgs e)
        {
            dt = ((DataView)MainDataGrid.ItemsSource).ToTable();

            var dataGrid = MainDataGrid;
            var columnWidths = new List<double>() { 75, 100, 150, 300, 100 }; //Gauge, Owner, Location, Desc, Due
            if (SearchQuery == "Vendors") { columnWidths = new List<double>() { 50, 150, 80, 150 }; }
            if (ExternalReport == true) { columnWidths = new List<double>() { 50, 150, 80, 80, 100, 150 }; }
            var ht = new HeaderTemplate();
            var headerTemplate = XamlWriter.Save(ht);
            var printControl = PrintControlFactory.Create(dt, columnWidths, headerTemplate);
            printControl.ShowPrintPreview();
            
        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MainDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (MainDataGrid.CurrentColumn.Header.ToString() == "Gauge")
            {
                object item = MainDataGrid.SelectedItem;
                string ID = MainDataGrid.CurrentColumn.GetCellContent(item).ToString();
                ID = cboParser(ID);
                EquipmentWindow equipmentWindow = new EquipmentWindow(ID)
                { Owner = App.Current.MainWindow };
                equipmentWindow.ShowDialog();
                MainDataGrid.SelectedItems.Clear();
            }
        }

        private static string cboParser(string controlString)
        {
            if (controlString.Contains(':'))
            {
                controlString = controlString.Split(':')[1].TrimStart(' ');
            }
            return controlString;
        }

        private async void DataGridCell_MouseEnter(object sender, MouseEventArgs e)
        {
            if(cts != null)
            {
                cts.Cancel();
            }
        
            System.Threading.CancellationTokenSource newCTS = new System.Threading.CancellationTokenSource();
            cts = newCTS;
        
            
            DataGridCell dataGridCell = (DataGridCell)sender;
            DataRowView drv = (DataRowView)MainDataGrid.SelectedItem;
            if (dataGridCell.Column.Header.ToString() == "Gauge")
            {
                string CellValue = cboParser(sender.ToString());
                try
        
                { await QuickInfoBar(cts.Token, CellValue); }
                catch(OperationCanceledException)
                { }
                if (cts == newCTS)
                    cts = null;
                
            }
            e.Handled = true;
        }

        async Task QuickInfoBar(System.Threading.CancellationToken ct, string CellValue)
        {
            await Task.Delay(400);
            ct.ThrowIfCancellationRequested();
            // ShowImage(CellValue);
            DetailBox_ID= string.Format("Guage ID: " + CellValue); // TB_GuageID.Visibility = Visibility.Visible;
            
        }

        // void ShowImage(string CellValue)
        // {
        //     try
        //     {
        //         // EquipImage.Source = new BitmapImage(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + GageTBox.Text + ".jpg", UriKind.Absolute));
        //         ImageSource src = CLS.BitmapFromUri(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\" + CellValue + ".jpg", UriKind.Absolute));
        //         EquipImage.Source = src;
        //     }
        //     catch
        //     {
        //         // EquipImage.Source = new BitmapImage(new Uri(@"V:\Users\GAGE CALIBRATION RECORDS\SOP7_6_PICTURES\EMPTY.jpg", UriKind.Absolute));
        //         EquipImage.Source = new BitmapImage(new Uri("EMPTY.jpg", UriKind.Relative));
        //     }
        // }
        
    }

    public class ValueToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string input = value as string;

            switch (input)
            {
                case "ASSEMBLY": return Brushes.Yellow;
                case "CERAMICS": return Brushes.Gray;
                case "MOLDING": return Brushes.Red;
                case "BRUSH": return Brushes.LightBlue;
                case "INSP/SHIPPING": return Brushes.Green;
                case "OVENS": return Brushes.Brown;

                default:
                    return DependencyProperty.UnsetValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            return DependencyProperty.UnsetValue;
        }
    }

}