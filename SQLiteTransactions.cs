﻿using System.Data.SQLite;
using System;
using System.Windows;
using CLS = Classes.IO_class;
using System.Data;
using System.Collections.Generic;

namespace SQLiteTrans
{

    public class SQLiteTransaction
    {

        private static SQLiteConnection m_dbConnection;
        public static void ReadOnlyPermission()
        {
            MessageBox.Show("The database is open as read only.\nContact your network administrator for write access if needed.");
        }

        public static void ConnectToDatabase()
        {
            // m_dbConnection = new SQLiteConnection("Data Source=SOP7_6.db;Version=3;");
            // m_dbConnection.Open();
            string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3");
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
         
            
            
        }

        public static void disconnectFromDB()
        {
            m_dbConnection.Close();
        }

        public static string QueryDB(string sql, string Query)
        {
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            string Result;
            Result = reader[Query].ToString();
            return Result;
        }

        public static void AddHistory(string Gauge, string Owner, string Location, string Due, string Insp, string DevAsRec, string DevAsRet, string Comments, string CalFreq, string Initials, string OrigDueDate, string FitForUse, string Cert)
        {
            ConnectToDatabase();
            string sql = "INSERT INTO History (Gauge, Owner, Location, Due, Insp, DevAsRec, DevAsRet, Comments, CalFreq, Initials, OriginalDue, FitForUse, Cert) values ('" + Gauge + "', '" + Owner + "', '" + Location + "', '" + Due + "', '" + Insp + "', '" +
                    DevAsRec + "', '" + DevAsRet + "', '" + Comments + "', '" + CalFreq + "', '" + Initials + "', '" + OrigDueDate + "', '" + FitForUse + "', '" + Cert + "')";
            SQLiteCommand commandAdd = new SQLiteCommand(sql, m_dbConnection);
            commandAdd.ExecuteNonQuery();
            disconnectFromDB();
        }

        public static DataTable QueryToDataTable(string sql)
        {
            ConnectToDatabase();
            DataTable dt = new DataTable();
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
            sda.Fill(dt);
            disconnectFromDB();
            return dt;
        }

        public static List<string> QueryToListString(string sql, string Column_name) // sql = "SELECT DISTINCT CAL_HOUSE FROM External"
        {
            List<string> results = new List<string>();
            ConnectToDatabase();
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    results.Add(Convert.ToString(reader[Column_name]));
                }
            }
            disconnectFromDB();
                return results;
        }

        public static void ExecuteNonQuery(string sql)
        {
            ConnectToDatabase();
            SQLiteCommand commandAdd = new SQLiteCommand(sql, m_dbConnection);
            commandAdd.ExecuteNonQuery();
            disconnectFromDB();
        }

    }
}
// use polymorphism to vary database interaction