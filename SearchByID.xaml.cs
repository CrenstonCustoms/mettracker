﻿// Completed 9/21/2017
// Written and tested by Daniel Robbins
using System;
using System.Windows;
using System.Windows.Input;
using System.Data.SQLite;
using CLS = Classes.IO_class;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for SearchByID.xaml
    /// </summary>
    public partial class SearchByID : Window
    {
        public SearchByID()
        {
            InitializeComponent();
        }

        SQLiteConnection m_dbConnection;

        // Connect to database
        void connectToDatabase()
        {
            string DBDirectory = CLS.GetSettingsStrings("DBDirectory").ToString();
            String dataPath = string.Format(DBDirectory + ";Version=3"); ;
            String ConnectionString = string.Format("Data Source={0}", dataPath);
            m_dbConnection = new SQLiteConnection(ConnectionString);
            m_dbConnection.Open();
        }

        // Check if gauge exists. If so, pass gauge number to equipmentwindow for display.
        // if not exist, display error message
        void checkExist()
        {
            try
            {
                connectToDatabase();

                String SearchByIDTbox = SearchByIDText.Text.ToString();
                string sql = "SELECT * FROM Equipment WHERE Gauge = '" + SearchByIDText.Text + "'";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                String SearchString = reader["Gauge"].ToString();

                if (SearchString == "")
                {
                    throw new Exception();
                }
                m_dbConnection.Close();
                EquipmentWindow equipmentWindow = new EquipmentWindow(SearchString)
                { Owner = App.Current.MainWindow };
                equipmentWindow.Owner = this;
                this.Hide();
                equipmentWindow.ShowDialog();
                this.Close();

            }
            catch
            {
                MessageBox.Show("The Gauge \"" + SearchByIDText.Text + "\" does not exist.");
            }
        }

        // listen for enter key press on guage number textbox
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                checkExist();
            }
        }

        public void SearchbyID_Click(object sender, RoutedEventArgs e)
        {
            checkExist();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}