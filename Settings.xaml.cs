﻿using System.Windows;
using CLS = Classes.IO_class;
using System;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            LoadSettings();
        }

        public void LoadSettings()
        {
            DBDirectory.Text = CLS.GetSettingsStrings("DBDirectory");
            SOPDirectory.Text = CLS.GetSettingsStrings("SOPDirectory");
            SOPCalDirectory.Text = CLS.GetSettingsStrings("SOPCalDirectory");
            WIDirectory.Text = CLS.GetSettingsStrings("WIDirectory");
            Cal_Cert_Directory.Text = CLS.Settings.CalCertDirectory;
            ExternalInterval.Text = CLS.Settings.ExternalCalInterval.ToString();
        }

        public void SaveSettings()
        {
            MetTracker.Properties.Settings.Default.DBDirectory = DBDirectory.Text;
            MetTracker.Properties.Settings.Default.SOPDirectory = SOPDirectory.Text;
            MetTracker.Properties.Settings.Default.WIDirectory = WIDirectory.Text;
            MetTracker.Properties.Settings.Default.SOPCalDirectory = SOPCalDirectory.Text;
            CLS.Settings.CalCertDirectory = Cal_Cert_Directory.Text;
            
                CLS.Settings.ExternalCalInterval = double.Parse(ExternalInterval.Text);
            
            MetTracker.Properties.Settings.Default.Save();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((double.Parse(ExternalInterval.Text) >= 1) && (double.Parse(ExternalInterval.Text) <= 365))
                { }
                else { throw new Exception(); }
                SaveSettings();
            }
            catch
            { MessageBox.Show("The value entered for External Reminder Interval must be a number between 1 and 365."); return; }

            
            MessageBox.Show("Any changes will not take effect until MetTracker is closed and restarted.");
            this.Close();
        }

        private void SelectDB_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                Title = "Select the .db file to use",
                Filter = "SQLite Database (*.db)|*.db"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                DBDirectory.Text = openFileDialog.FileName;
            }
        }

        private void SelectSOP_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowDialog();
            string result = dialog.SelectedPath;
            // MessageBox.Show(result.ToString()); // for debugging
            SOPDirectory.Text = result.ToString();
        }

        private void SelectSOPCal_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowDialog();
            string result = dialog.SelectedPath;
            // MessageBox.Show(result.ToString()); // for debugging
            SOPCalDirectory.Text = result.ToString();
        }

        private void SelectWI_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowDialog();
            string result = dialog.SelectedPath;
            // MessageBox.Show(result.ToString()); // for debugging
            WIDirectory.Text = result.ToString();
        }

        private void CalCertSelect_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowDialog();
            string result = dialog.SelectedPath;
            // MessageBox.Show(result.ToString()); // for debugging
            Cal_Cert_Directory.Text = result.ToString();
        }
    }
}