﻿using System.Windows;
using System.Windows.Navigation;
using System.Diagnostics;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class Support : Window
    {
        public Support()
        {
            InitializeComponent();
        }

        private void OnNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }

        private void OK_Btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
