﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MetTracker
{
    /// <summary>
    /// Interaction logic for TextMessageBox.xaml
    /// </summary>
    public partial class TextMessageBox : Window
    {
        public TextMessageBox()
        {
            InitializeComponent();
        }
        
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Properties["NewLocation"] = TextBox1.Text;
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    
}
