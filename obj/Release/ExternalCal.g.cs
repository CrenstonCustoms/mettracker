﻿#pragma checksum "..\..\ExternalCal.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6F7F3C74DE1049BDE4DF72B438B200FD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MetTracker;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MetTracker {
    
    
    /// <summary>
    /// ExternalCal
    /// </summary>
    public partial class ExternalCal : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ExternalCalGrid;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem Equipment;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid EquipListDG;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem Details;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid DetailsGrid;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem CalHouseTab;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid CalHouseGrid;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CalHouse_CB;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ButtonGrid;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add_Equip_Btn;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add_CalHouse_Btn;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\ExternalCal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Close_Btn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MetTracker;component/externalcal.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ExternalCal.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ExternalCalGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.Equipment = ((System.Windows.Controls.TabItem)(target));
            
            #line 16 "..\..\ExternalCal.xaml"
            this.Equipment.GotFocus += new System.Windows.RoutedEventHandler(this.Equipment_GotFocus);
            
            #line default
            #line hidden
            return;
            case 3:
            this.EquipListDG = ((System.Windows.Controls.DataGrid)(target));
            
            #line 20 "..\..\ExternalCal.xaml"
            this.EquipListDG.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.EquipListDG_MouseDoubleClick);
            
            #line default
            #line hidden
            
            #line 22 "..\..\ExternalCal.xaml"
            this.EquipListDG.RowEditEnding += new System.EventHandler<System.Windows.Controls.DataGridRowEditEndingEventArgs>(this.resultGrid_CellEditEnding);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Details = ((System.Windows.Controls.TabItem)(target));
            
            #line 27 "..\..\ExternalCal.xaml"
            this.Details.GotFocus += new System.Windows.RoutedEventHandler(this.Details_GotFocus);
            
            #line default
            #line hidden
            return;
            case 5:
            this.DetailsGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.CalHouseTab = ((System.Windows.Controls.TabItem)(target));
            return;
            case 7:
            this.CalHouseGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.CalHouse_CB = ((System.Windows.Controls.ComboBox)(target));
            
            #line 48 "..\..\ExternalCal.xaml"
            this.CalHouse_CB.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CalHouse_CB_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.ButtonGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.Add_Equip_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\ExternalCal.xaml"
            this.Add_Equip_Btn.Click += new System.Windows.RoutedEventHandler(this.Add_Equip_Btn_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.Add_CalHouse_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\ExternalCal.xaml"
            this.Add_CalHouse_Btn.Click += new System.Windows.RoutedEventHandler(this.Add_CalHouse_Btn_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.Close_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\ExternalCal.xaml"
            this.Close_Btn.Click += new System.Windows.RoutedEventHandler(this.Close_Btn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

