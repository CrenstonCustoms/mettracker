﻿#pragma checksum "..\..\ReportsPrintable.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "889AA3EBBE29CF3668461F1DFB9178AF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MetTracker;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MetTracker {
    
    
    /// <summary>
    /// ReportsPrintable
    /// </summary>
    public partial class ReportsPrintable : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 18 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MenuExit;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MenuPrint;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid MainDataGrid;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image EquipImage;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TB_GuageID;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TB_Description;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TB_CalDate;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TB_DueDate;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TB_Comments;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\ReportsPrintable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TB_Details;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MetTracker;component/reportsprintable.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ReportsPrintable.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MenuExit = ((System.Windows.Controls.MenuItem)(target));
            
            #line 18 "..\..\ReportsPrintable.xaml"
            this.MenuExit.Click += new System.Windows.RoutedEventHandler(this.MenuExit_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.MenuPrint = ((System.Windows.Controls.MenuItem)(target));
            
            #line 19 "..\..\ReportsPrintable.xaml"
            this.MenuPrint.Click += new System.Windows.RoutedEventHandler(this.MenuPrint_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.MainDataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 23 "..\..\ReportsPrintable.xaml"
            this.MainDataGrid.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.MainDataGrid_MouseDoubleClick);
            
            #line default
            #line hidden
            
            #line 24 "..\..\ReportsPrintable.xaml"
            this.MainDataGrid.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.MainDataGrid_AutoGeneratingColumn);
            
            #line default
            #line hidden
            
            #line 24 "..\..\ReportsPrintable.xaml"
            this.MainDataGrid.RowEditEnding += new System.EventHandler<System.Windows.Controls.DataGridRowEditEndingEventArgs>(this.MainDataGrid_CellEditEnding);
            
            #line default
            #line hidden
            return;
            case 5:
            this.EquipImage = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.TB_GuageID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.TB_Description = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.TB_CalDate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TB_DueDate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.TB_Comments = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.TB_Details = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 4:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.UIElement.MouseEnterEvent;
            
            #line 28 "..\..\ReportsPrintable.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseEventHandler(this.DataGridCell_MouseEnter);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

